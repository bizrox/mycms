<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 08.02.2016
 * Time: 20:34
 */

// Login
$lang['journal_heading']         = 'Jurnal';
$lang['nutrition']         = 'Nutritie';
$lang['evolution']         = 'Evolutie';
$lang['gym_plans']         = 'Program sala';
$lang['todays_macro']	   = 'Macronutrientii pe ziua de azi';
$lang['proteins']         = 'Proteine';
$lang['carbohydrates']         = 'Carbohidrati';
$lang['fats']         = 'Grasimi';
$lang['calories']         = 'Calorii';