<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/12/2015
 * Time: 3:00 PM
 */

class Menus {

    public $languageSlug;

    private $parentIds = array();
    private $menu = null;
    protected $CI = null;

    public function __construct() {
        $this->CI =& get_instance();

        $this->CI->load->model('menu_model');
        $this->languageSlug = $this->CI->session->set_language;
    }

    public function getMenu($menuName, $languageSlug = NULL, $return = 'array', $loggedIn = false) {
        $languageSlug = (is_null($languageSlug)) ? $this->language_slug : $languageSlug;

        $menus = $this->CI->menu_model->getAll();

        if(!empty($menus))
        {
            foreach($menus as $menu)
            {//var_dump(strtolower(str_replace(' ', '', $menu->title)));die();
                if(strtolower(str_replace(' ', '', $menu->title)) == strtolower($menuName))
                {
                    $menuId = $menu->id;
                    break;
                }
            }

            if(!isset($menuId))
            {
                return FALSE;
            }

            //$this->menu_item_model->db->order_by('order','ASC');

            $menuItems = $this->CI->menu_model->getMenuItems($menuId);
            //$logged_in_only = '1';
            $theMenu = array();

           /* var_dump( $loggedIn );
            var_dump( $menuItems );die();*/
            if(!empty($menuItems))
            {

                //before we create the menu we need to iterate through the menu items
                //and exclude or include the menu items that are available to logged in users only
                //or logged out users only or both
                foreach ( $menuItems as $index => $item ) {

                    if ( $loggedIn ) {//he is logged in
                        if ( $item->logged_out_only && $item->logged_in_only ) {//for both logged in and logged out
                            continue;
                        } elseif ( !$item->logged_out_only && $item->logged_in_only ) {//for logged in only
                            continue;
                        } else {//the item is available for logged out users only
                            unset($menuItems[$index]);
                        }
                    } elseif ( !$loggedIn ) {//nope. He is not logged out.
                        if ( $item->logged_out_only && $item->logged_in_only ) {//for both logged in and logged out
                            continue;
                        } elseif ( !$item->logged_in_only && $item->logged_out_only ) {//for logged out only
                            continue;
                        } else {//the item is available for logged in users only
                            unset($menuItems[$index]);
                        }
                    }

                }


                foreach($menuItems as $item)
                {
                    if(!empty($item->translations))
                    {
                        foreach ($item->translations as $translation)
                        {
                            if($translation->language_slug == $languageSlug) {
                                $url = ($translation->absolute_path == '1') ? $translation->url :  site_url($this->languageSlug . $translation->url);
                                $theMenu[] = array('id' => $item->id, 'parent_id' => $item->parent_id, 'title' => $translation->title, 'url' => $url, 'css_class' => $item->styling);
                            }
                        }
                    }
                }
            }
            /*echo '<pre>';
            print_r($theMenu);die();*/
            foreach ($theMenu as $key => $menuItem)
            {
                $this->parentIds[] = $menuItem['parent_id'];
            }
            $this->parentIds = array_unique($this->parentIds);
            $this->menu = $theMenu;
            // 0 - parent
            
            return $this->generateMenu(0);

            /*switch ($return) {
                case 'array':
                    $the_menu = $this->ordered_list($the_menu);
                    break;
                case 'html_menu': {
                    $the_menu = $this->html_menu($the_menu);
                }
                case 'bootstrap_menu': {
                    $the_menu = $this->generate_menu(0);
                }
            }*/
            //return $the_menu;
        }
    }

    function generateMenu($parent)
    {//die("generate");
        $has_childs = false;

        //this prevents printing 'ul' if we don't have subcategories for this category
        global $menuItems;
        global $parentMenuIds;

        $menu = '';
        //use global array variable instead of a local variable to lower stack memory requierment
        foreach($this->menu as $key => $value)
        {
           // print_r($value);
            if ($value['parent_id'] == $parent)
            {
                //if this is the first child print '<ul>'
                if ($has_childs === false)
                {
                    //don't print '<ul>' multiple times
                    $has_childs = true;
                    if($parent != 0)
                    {
                        //echo '<ul class="dropdown-menu">';
                        $menu .= '<ul class="dropdown-menu">';
                    }
                }

                if($value['parent_id'] == 0 && in_array($value['id'], $this->parentIds))
                {
                    //echo '<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">' . $value['title'] . '<b class="caret"></b></a>';
                    $menu .= '<li class="dropdown"><span class="arrow-bottom green"></span><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="'.$value['css_class'].'"></i><span>' . $value['title'] . '</span></a>';
                }
                else if($value['parent_id'] != 0 && in_array($value['id'], $this->parentIds))
                {
                    //echo '<li class="dropdown-submenu"><a href="#">' . $value['title'] . '</a>';
                    $menu .= '<li class="dropdown-submenu"><span class="arrow-bottom green"></span><a href="'.$value['url'].'"><span>' . $value['title'] . '</span><b class="right-caret"></b></a>';
                }
                else
                {//no childs
                    //echo '<li><a href="#">' . $value['title'] . '</a>';
                    $menu .= '<li><span class="arrow-bottom green"></span><a href="'.$value['url'].'"><i class="'.$value['css_class'].'"></i><span>' . $value['title'] . '</span></a>';
                }
                $menu .= $this->generateMenu($value['id']);

                //call function again to generate nested list for subcategories belonging to this category
                //echo '</li>';
                $menu .= '</li>';
            }
        }
        if ($has_childs === true)  $menu .='</ul>';//echo '</ul>';
        return $menu;
    }
}