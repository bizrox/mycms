<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/2/2015
 * Time: 4:27 PM
 */

class Menu_model extends CI_Model
{
    const CACHE_PREFIX = 'menu_v';

    function __construct()
    {
        parent::__construct();
    }

    public function getAll($where = NULL)
    {
        if(isset($where))
        {
            $this->db->where($where);
        }

        $query = $this->db->get('menus');

        if( $query->num_rows() > 0 )
        {
            return $query->result();
        }

        return FALSE;
    }

    public function saveMenu($data = array())
    {
        if ($data)
        {
            $this->db->insert('menus', $data);

            if ( $this->db->affected_rows() == 1 )
            {
                return true;
            }

            return false;
        }
        return false;
    }

    public function updateMenu( $data = array(), $id )
    {
        if ( $data && $id )
        {
            $this->db->where('id', $id);

            $success = $this->db->update('menus', $data);
            //var_dump($success);die();

            if ( $success )
            {
                return true;
            }

            return false;
        }
        return false;
    }

    public function deleteMenu($id)
    {
        //delete menu items and translations
        $menuItemsTrans = $this->getMenuItems($id);
        foreach($menuItemsTrans as $menuItem)
        {
            $this->deleteMenuItemTrans($menuItem->id);
        }
        if ( $this->deleteMenuItemsByMenuId($id) )
        {
            return $this->db->delete('menus', array('id' => $id));
        }

    }

    public function deleteMenuItemTrans($itemId)
    {
        $this->db->delete('menu_item_translations', array('item_id' => $itemId));

        if ( $this->db->affected_rows() )
        {
            return true;
        }

        return false;
    }

    public function deleteMenuItemTransLang($itemId, $lang)
    {
        $this->db->delete('menu_item_translations', array('item_id' => $itemId, 'language_slug' => $lang));

        if ( $this->db->affected_rows() )
        {
            return true;
        }

        return false;
    }

    public function deleteMenuItems($id)
    {

        $this->db->delete('menu_items', array('id' => $id));

        if ( $this->db->affected_rows() )
        {
            //make sure we delete the old cache data
            $menuItemsCacheKey = self::CACHE_PREFIX .'items';
            if ( $this->cache->get($menuItemsCacheKey) ) {
                $this->cache->delete($menuItemsCacheKey);
            }
            return true;
        }

        return false;
    }

    public function deleteMenuItemsByMenuId($id)
    {

        $this->db->delete('menu_items', array('menu_id' => $id));

        if ( $this->db->affected_rows() )
        {
            return true;
        }

        return false;
    }

    public function getById($id = null)
    {
        if(isset($id) && is_int($id))
        {
            $this->db->where('id', $id);
            $query = $this->db->get('menus');
            //var_dump($query->row());die();
            if($query->num_rows() == 1)
            {
                return $query->row();
            }
        }
        return FALSE;
    }

    public function getMenuItemById( $id = null, $slug = null )
    {
        if(isset($id) && is_int($id))
        {
            $query = $this->db->select('*')
                                ->from('menu_items')
                                ->join('menu_item_translations', 'menu_items.id = menu_item_translations.item_id')
                                ->where('menu_items.id', $id)
                                ->where('menu_item_translations.language_slug', $slug)
                                ->get();
            //$query = $this->db->get('menu_items');
            //var_dump($query->row());die();
            if($query->num_rows() > 0)
            {
                return $query->result();
            }
        }
        return FALSE;
    }

    public function getTranslationsByItemId($itemId)
    {
        $query = $this->db->select('id, language_slug, title, url, absolute_path')
                            ->from('menu_item_translations')
                            ->where('item_id', $itemId)
                            ->get();
        return $query->result();
    }

    public function getTranslationsByItemIdAndSlug($itemId, $slug)
    {
        $query = $this->db->select('id, language_slug, title, url, absolute_path')
            ->from('menu_item_translations')
            ->where('item_id', $itemId)
            ->where('language_slug', $slug)
            ->get();
        return $query->result();
    }

    public function getMenuItems($menuId, $limit = 0, $offset = 0)
    {
        $menuItemsCacheKey = self::CACHE_PREFIX .'items';

        if ( $menuItems = $this->cache->get($menuItemsCacheKey) ) {/*echo "<pre>";
            print_r($menuItems);die();*/
            //print_r($menuItems);die();
            return $menuItems;
        } else {
            //die("recreate cache");
            $query = ' select * from menu_items where menu_id = '.$this->db->escape($menuId). ' order by menu_items.order ASC ';

            /*if ( $loggedIn ) {
                $query .= ' and logged_in_only = 1';
            }*/

            if ( $limit || $offset )
            {
                $query .= 'Limit '. (int)$offset . ', ' . (int)$limit;
            }
            $query = $this->db->query($query);
            /* $query = $this->db->select('*')
                      ->from('menu_items')
                      //->join('menu_item_translations', 'menu_items.id = menu_item_translations.item_id')
                      ->where('menu_id', $menuId)
                      //->group_by('menu_item_translations.language_slug')
                      ->get();*/
            //$this->db
            //$query = $this->db->get('menus');
            //var_dump($query->result());die();

            if($query->num_rows() > 0)
            {
                //$items = $query->result();

                foreach ( $query->result() as &$item )
                {//get translations
                    //$this->getTranslationsByItemId($item->item_id);
                    $translations = $this->getTranslationsByItemId($item->id);
                    if ( $translations )
                    {
                        foreach ( $translations as $translate )
                        {
                            $item->translations[] = $translate;
                        }
                    }
                }
                //cache it for 10 days
                $this->cache->write($query->result(), self::CACHE_PREFIX .'items', 864000);

                return $query->result();
            }
        }

    }

    public function getAllItemsWithTranslations($slug, $itemId = null)
    {
        if ( $itemId !== null )
        {
            $query = $this->db->select("item_id, id, title, url, absolute_path")
                ->where('language_slug', $slug)
                    ->where('item_id !=', $itemId)
                ->get('menu_item_translations');
        }
        else
        {
            $query = $this->db->select("item_id, id, title, url, absolute_path")
                ->where('language_slug', $slug)
                ->get('menu_item_translations');
        }

        if( $query->num_rows() > 0 )
        {
            return $query->result();
        }

        return FALSE;
    }

    public function insertItemMenu($data)
    {
        if ( $data )
        {
            $this->db->insert('menu_items', $data);

            if ( $this->db->affected_rows() == 1 )
            {
                //make sure we delete the old cache data
                $menuItemsCacheKey = self::CACHE_PREFIX .'items';
                if ( $this->cache->get($menuItemsCacheKey) ) {
                    $this->cache->delete($menuItemsCacheKey);
                }
                return $this->db->insert_id();
            }

            return false;
        }
        return false;
    }

    public function insertItemMenuTrans($data)
    {
        if ( $data )
        {
            $this->db->insert('menu_item_translations', $data);

            if ( $this->db->affected_rows() == 1 )
            {
                //make sure we delete the old cache data
                $menuItemsCacheKey = self::CACHE_PREFIX .'items';
                if ( $this->cache->get($menuItemsCacheKey) ) {
                    $this->cache->delete($menuItemsCacheKey);
                }
                return $this->db->insert_id();
            }

            return false;
        }
        return false;
    }

    public function updateItemMenu($data, $id)
    {
        if ( $data && $id )
        {
            $this->db->where('id', $id);

            $success = $this->db->update('menu_items', $data);
            //var_dump($success);die();

            if ( $success )
            {
                //make sure we delete the old cache data
                $menuItemsCacheKey = self::CACHE_PREFIX .'items';
                if ( $this->cache->get($menuItemsCacheKey) ) {
                    $this->cache->delete($menuItemsCacheKey);
                }
                return true;
            }

            return false;
        }
        return false;
    }

    public function updateItemMenuTrans($data, $id, $langSlug)
    {   //var_dump($id);die();
        if ( $data && $id )
        {
            $this->db->where('id', $id);
            $this->db->where('language_slug', $langSlug);

            $success = $this->db->update('menu_item_translations', $data);
            //var_dump($success);die();

            if ( $success )
            {
                //make sure we delete the old cache data
                $menuItemsCacheKey = self::CACHE_PREFIX .'items';
                if ( $this->cache->get($menuItemsCacheKey) ) {
                    $this->cache->delete($menuItemsCacheKey);
                }
                return true;
            }

            return false;
        }
        return false;
    }
}