<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public $tableColumns = array(
    							'macro_id',
    							'modified_on',
    							'user_id',
    							'macro_value'
    						);

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
    }
    /**
     * this will get the daily macros for the user
     * @param   $userId
     * @return  results set or false on failure      
     */
    public function getMacrosByUserId($userId)
    {
    	if ( $userId > 0 )
    	{
    		//run the query 
    		$query = $this->db->select('*')
    						  ->from('users_macro_values')
    						  ->join('macros', 'users_macro_values.macro_id = macros.macro_id')
    						  ->where('user_id', $userId)
    						  ->where('created_on', date("Y-m-d"))
    						  ->get();
    		//do we have data?
    		//var_dump($this->db->last_query());die();
    		if ( $query->num_rows() > 0 )
    		{		/**/
    			return $query->result_array();
    		} 
    		else
    		{/*die("here");*/
    			return false;
    		}

    	}
    	return false;
    }
    /**
     * save macros entered by user
     */
    public function saveMacros($userId, $macros)
    {	
    	//first check if the user already updated his macros today
    	if ( $this->dashboard_model->getMacrosByUserId($userId) )
    	{   
    	    $this->db->trans_begin();
    	    
    		foreach ($macros as $macro) {
    			$this->db->where('user_id', $userId);
    			$this->db->where('macro_id', $macro['macro_id']);
    			$this->db->where('created_on', date("Y-m-d"));
				$this->db->update('users_macro_values', $macro);
    		}
    		//making sure the update worked
    		if ($this->db->trans_status() === false)
            { 
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }
            
    		return true;
    	}
    	else 
    	{//insert :D
    	    $this->db->trans_begin();
    	    
    	    $this->db->insert_batch('users_macro_values', $macros);
    	    //making sure the update worked
    		if ($this->db->trans_status() === false)
            { 
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }  
	  		
	  		return true;
    	}
  		
    }
    /*
     * this function will select and group by "created_on" the last 7 days of the user macros
     */
    public function getUserSevenDaysStatus($userId)
    {
       if ( $userId > 0 )
       { //, "," , `users_macro_values`.`created_on`)
            $sql = '  SELECT GROUP_CONCAT( concat( `macros`.`name` , "," , `users_macro_values`.`macro_value` )) macros, `users_macro_values`.`created_on` 
                        FROM `users_macro_values`
                        JOIN `macros` ON `users_macro_values`.`macro_id` = `macros`.`macro_id`
                        WHERE `user_id` = ' . $this->db->escape($userId) . ' and `users_macro_values`.`created_on` < CURDATE()
                        GROUP BY `users_macro_values`.`created_on`
                        ORDER BY `users_macro_values`.`created_on` DESC
                        LIMIT 7';
            $query = $this->db->query($sql);
            
    		if ( $query->num_rows() > 0 )
    		{	
    			return $query->result_array();
    		} 
    		else
    		{
    			return false;
    		}
       }
       return false;
    }
}