<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/16/2015
 * Time: 2:03 PM
 */

class Pages_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getAll($where = NULL)
    {
        if(isset($where))
        {
            $this->db->where($where);
        }

        $query = $this->db->get('pages');

        if( $query->num_rows() > 0 )
        {
            return $query->result();
        }

        return FALSE;
    }

    public function getPageById( $id = null, $slug = null )
    {
        if(isset($id) && is_int($id))
        {
            $query = $this->db->select('*')
                ->from('pages')
                ->join('page_translations', 'pages.id = page_translations.page_id')
                ->where('pages.id', $id)
                ->where('page_translations.language_slug', $slug)
                ->get();
            //$query = $this->db->get('menu_items');
            //var_dump($query->row());die();
            if($query->num_rows() > 0)
            {
                return $query->result();
            }
        }
        return FALSE;
    }

    public function insertPage($data)
    {
        if ($data)
        {
            $this->db->insert('pages', $data);

            if ( $this->db->affected_rows() == 1 )
            {
                return $this->db->insert_id();
            }

            return false;
        }
        return false;
    }

    public function insertPageTrans($data)
    {
        if ( $data )
        {
            $this->db->insert('page_translations', $data);

            if ( $this->db->affected_rows() == 1 )
            {
                return $this->db->insert_id();
            }

            return false;
        }
        return false;
    }
}