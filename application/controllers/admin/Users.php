<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 10/30/2015
 * Time: 2:16 PM
 */
class Users extends Admin_Controller
{

    function __construct() {
        parent::__construct();
        //only the users in the admin group can edit the groups
        if(!$this->ion_auth->in_group('admin')) {
            $this->session->set_flashdata('message', 'You are not allowed to visit the Users page');
            redirect('admin', 'refresh');
        }
    }

    public function index( ) {}

    public function list_users() {
        $this->data['page_title'] = 'Users';
        //pagination settings
        //$config['base_url'] = base_url() . 'admin/users/list_users';
        //$config['total_rows'] = $this->ion_auth->users()->num_rows();
        //$config['per_page'] = $this->settings->per_page_limit;


        //$this->pagination->initialize($config);
        //$config["uri_segment"] = ($this->uri->segment(4)) ? (int)$this->uri->segment(4) : 0;
        //$data2['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        //var_dump($config["uri_segment"]);die();
        //list the users
        //$this->data['users'] = $this->ion_auth->offset($config["uri_segment"])->limit($config['per_page'])->users()->result();
        $this->data['users'] = $this->ion_auth->users()->result();
        // var_dump($this->ion_auth->users()->num_rows());die();
        //call the model function to get the department data
        //$content_data['projects'] = $this->project_model->getAllProjects($config["per_page"], $data2['page']);

        //$this->data['pagination'] = $this->pagination->create_links();
        //$this->data['users'] = $this->ion_auth->users($groupId)->result();
        $this->render('admin/users/list_users');
    }

    public function profile() {

        $this->data['page_title'] = 'My Profile';
        $user = $this->ion_auth->user()->row();
        $this->data['user'] = $user;

        $this->form_validation->set_rules('first_name', 'First name','trim');
        $this->form_validation->set_rules('last_name', 'Last name','trim');
        $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
        //$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');
        //$this->form_validation->set_rules('company','Company','trim');
        //$this->form_validation->set_rules('phone','Phone','trim');

        // update the password if it was posted
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Password confirm', 'required');
        }

        if($this->form_validation->run() === TRUE) {
            $new_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email'     => $this->input->post('email')
                //'company' => $this->input->post('company'),
                //'phone' => $this->input->post('phone')
            );
            if ($this->input->post('password')) {
                $new_data['password'] = $this->input->post('password');
            }
            $this->ion_auth->update($user->id, $new_data);

            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('admin/users/profile', 'refresh');
        } else {
            $this->render('admin/users/profile');
        }
    }

    public function create() {

        $this->data['page_title'] = 'New user';

        $tables = $this->config->item('tables', 'ion_auth');
        $identity_column = $this->config->item('identity', 'ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
        if($identity_column !== 'email') {
            $this->form_validation->set_rules('identity',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        } else {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
        //$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
        //$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

        if ($this->form_validation->run() == true) {
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');
            $groupIds = $this->input->post('groups');
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
                //'company'    => $this->input->post('company'),
                //'phone'      => $this->input->post('phone'),
            );

        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data, $groupIds))
        {
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("admin/users", 'refresh');
        } else {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['identity'] = array(
                'name'  => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['groups'] = $this->ion_auth->groups()->result();
            //var_dump($this->data['groups']);die();
            $groupNames = array();
            foreach ( $this->data['groups'] as $group ) {
                $groupNames[$group->id] = $group->name;
            }
            //var_dump($groupNames);die();
            $this->data['group_names'] = $groupNames;
            $this->render('admin/users/form');
        }

    }

    public function edit($id = null) {
        // make sure we have a numeric id
        if (is_null($id) OR !is_numeric($id)) {
            redirect('admin/users');
        }

        $userId = $id;
        $this->data['page_title'] = 'Edit user';

        $this->form_validation->set_rules('first_name','First name','trim|required');
        $this->form_validation->set_rules('last_name','Last name','trim|required');
        //$this->form_validation->set_rules('company','Company','trim');
        //$this->form_validation->set_rules('phone','Phone','trim');
        //$this->form_validation->set_rules('username','Username','trim|required');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email');

        $this->form_validation->set_rules('groups[]','Groups','required|integer');

        if ( $this->input->post('password') )
        {
            $this->form_validation->set_rules('password','Password','required|min_length[6]');
            $this->form_validation->set_rules('confirm_password','Password confirmation','required|matches[password]');
        }

        if($this->form_validation->run() === FALSE) {
            if($user = $this->ion_auth->user($userId)->row()) {
                $this->data['groups'] = $this->ion_auth->groups()->result();
                $groupNames = array();
                foreach ( $this->data['groups'] as $group ) {
                    $groupNames[$group->id] = $group->name;
                }
                //var_dump($groupNames);die();
                $this->data['group_names'] = $groupNames;
                $this->data['user_groups'] = $this->ion_auth->get_users_groups($userId)->result();
                $userGroups = array();
                foreach ( $this->data['user_groups'] as $userGroup ) {
                    $userGroups[] = $userGroup->id;
                }
                //var_dump($userGroups);die();
                $this->data['user_groups'] = $userGroups;
                $this->data['group_names'] = $groupNames;
                $this->data['user'] = $user;
            } else {
                $this->session->set_flashdata('message', 'The user doesn\'t exist.');
                redirect('admin/users', 'refresh');
            }
            $this->render('admin/users/form');
        } else {

            $userId = $this->input->post('user_id');
            $newData = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'password'  => $this->input->post('password'),
                //'company' => $this->input->post('company'),
                //'phone' => $this->input->post('phone')
            );/*
            if(strlen($this->input->post('password'))>=6) $new_data['password'] = $this->input->post('password');*/

            $this->ion_auth->update($userId, $newData);

            //Update the groups user belongs to
            $groups = $this->input->post('groups');
            //var_dump($groups);die();
            if (isset($groups) && !empty($groups)) {
                $this->ion_auth->remove_from_group('', $userId);
                foreach ($groups as $group) {
                    $this->ion_auth->add_to_group($group, $userId);
                }
            }

            $this->session->set_flashdata('message',$this->ion_auth->messages());
            redirect('admin/users','refresh');
        }

    }

    public function delete($id = null) {
        // make sure we have a numeric id
        if (is_null($id) OR !is_numeric($id)) {
            redirect('admin/users');
        }

        $this->ion_auth->delete_user($id);
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('admin/users','refresh');
    }
}