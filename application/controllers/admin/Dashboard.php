<?php
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 10/30/2015
 * Time: 11:13 AM
 */

//namespace Admin\Dashboard;


class Dashboard extends Admin_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->render('admin/dashboard/index');
    }
}