<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('settings_model');
        // load the language files
        //$this->lang->load('settings');
    }


    /**
     * Settings Editor
     */
    function index()
    {
        // get settings
        $settings = $this->settings_model->get_settings();

        // form validations

        foreach ($settings as $setting)
        {
            if ($setting['validation'])
            {
                $this->form_validation->set_rules($setting['name'], $setting['label'], $setting['validation']);
            }
        }

        if ($this->form_validation->run() == TRUE)
        {
            // save the settings
            $saved = $this->settings_model->save_settings($this->input->post(), $this->userId);

            if ($saved)
            {
                $this->session->set_flashdata('message', "settings saved");

                // reload the new settings
                $settings = $this->settings_model->get_settings();
                foreach ($settings as $setting)
                {
                    $this->settings->{$setting['name']} = $setting['value'];
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('admin settings error save_failed'));
            }

            // reload the page
            redirect('admin/settings');
        }

        $this->data['page_title'] = 'Settings';

        $this->data['settings'] = $settings;
        $this->render('admin/settings/list_settings');
    }

}
