<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/2/2015
 * Time: 4:22 PM
 */
class Menus extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->in_group('admin')) {
            $this->session->set_flashdata('message', 'Your are not allowed here!');
            redirect('admin', refresh);
        }
        $this->load->model('menu_model');
        $this->load->model('language_model');
    }

    public function index()
    {
        $this->data['page_title'] = 'Menus';
        $this->data['menus'] = $this->menu_model->getAll();
        $this->render('admin/menus/list_menus');
    }

    /**
     * create a new menu
     */
    public function create()
    {
        $this->data['page_title'] = 'Create new menu';

        $this->form_validation->set_rules('title', 'Title', 'trim|required');

        if ($this->form_validation->run() === false) {
            $this->render('admin/menus/form');
        } else {
            $insertData = array(
                'title' => $this->input->post('title'),
                'created_by' => $this->userId,
                'created_at' => date('Y-m-d H:i:s')
            );

            if ($this->menu_model->saveMenu($insertData)) {
                $this->session->set_flashdata('message', 'The menu was created successfully');
                redirect('admin/menus');
            }

        }
    }

    public function edit($id = null)
    {
        // make sure we have a numeric id
        if (is_null($id) || !is_numeric($id)) {
            redirect('admin/menus');
        }

        $menuId = (int)$id;
        $this->data['menu'] = $this->menu_model->getById($menuId);

        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'trim|required');

        if ($this->form_validation->run() === false) {
            $this->render('admin/menus/form');
        } else {
            $updateData = array(
                'title' => $this->input->post('title'),
                'updated_by' => $this->userId,
            );
            //var_dump($updateData);die();
            if ($this->menu_model->updateMenu($updateData, $menuId)) {
                $this->session->set_flashdata('message', 'Menu successfully updated.');
                redirect('admin/menus');
            }
        }
    }

    public function delete($id = null)
    {
        // make sure we have a numeric id
        if (is_null($id) OR !is_numeric($id)) {
            redirect('admin/menus');
        }

        if ($this->menu_model->deleteMenu((int)$id)) {
            $this->session->set_flashdata('message', 'Menu was deleted successfully');
            redirect('admin/menus');
        } else {
            $this->session->set_flashdata('error', 'Menu could not be delted');
            redirect('admin/menus');
        }

    }

    public function items($menuId = null)
    {
        // make sure we have a numeric id
        if (is_null($menuId) OR !is_numeric($menuId)) {
            redirect('admin/menus');
        }
        $menuId = (int)$menuId;
        $this->data['menu'] = $this->menu_model->getById($menuId);
        $this->data['page_title'] = 'Menu items';
        //var_dump($this->menu_model->getById((int)$menuId));die();
        $listItems = array();
        //pagination settings
        $config['base_url'] = base_url() . 'admin/menus/items/' . $menuId . '/';
        $config['total_rows'] = $this->db->where('menu_id', $menuId)->count_all_results('menu_items');
        $config['per_page'] = $this->settings->per_page_limit;

        $this->pagination->initialize($config);
        $config["uri_segment"] = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $items = $this->menu_model->getMenuItems($menuId, $config['per_page'], $config["uri_segment"]);

        $this->data['pagination'] = $this->pagination->create_links();
        if ($items) {
            foreach ($items as $item) {
                $listItems[$item->id] = array(
                    'menu_id' => $item->menu_id,
                    'parent_id' => $item->parent_id,
                    'created_at' => $item->created_at,
                    'last_update' => $item->updated_at,
                    'deleted' => $item->deleted_at,
                    'translations' => array(),
                    'title' => ''
                );
                //var_dump($listItems);die();
                if (isset($item->translations)) {
                    foreach ($item->translations as $translation) {
                        $listItems[$item->id]['translations'][$translation->language_slug] = array(
                            'translation_id' => $translation->id,
                            'title' => $translation->title,
                            'url' => $translation->url
                        );
                        if (!isset($listItems[$item->id]['title'])) {
                            $listItems[$item->id]['title'] = $translation->title;
                        }
                        if (($translation->language_slug == $this->default_code_lang) && (strlen($translation->title) > 0)) {
                            $listItems[$item->id]['title'] = $translation->title;
                        }
                    }
                }
            }
        }

        $this->data['items'] = $listItems;
        //$this->data['next_previous_pages'] = $this->menu_item_model->all_pages;
        $this->render('admin/menus/items/list_menu_items');
    }

    /**
     * This method creates an item for a menu or edit it for a language
     * when the itemid is 0 we are creating a new item else editing existing one
     * @param null $menuId
     * @param null $languageSlug
     * @param int $itemId
     */
    public function createItem($menuId = null, $languageSlug = null, $itemId = 0)
    {
        // make sure we have a numeric id
        if (is_null($menuId) || !is_numeric($menuId) || $languageSlug == null) {
            redirect('admin/menus');
        }

        $this->data['menu_id'] = (int)$menuId;
        $languageSlug = (array_key_exists($languageSlug, $this->langs)) ? $languageSlug : $this->current_lang;

        $this->data['page_title'] = 'Add Item in ' . $this->langs[$languageSlug]['name'] . ' ';
        //var_dump($this->data['content_language']);die();
        $this->data['language_slug'] = $languageSlug;
        $item = $this->menu_model->getMenuItemById($itemId, $languageSlug);
        $onlytrans = false;

        if ($itemId != 0 && $item == false) {
            $originalId = $itemId;
            $itemId = 0;
            $onlytrans = true;
        }
        /*if($this->menu_item_translation_model->where(array('item_id'=>$item_id,'language_slug'=>$languageSlug))->get())
        {
            $this->postal->add('A translation for that menu item already exists.','error');
            redirect('admin/menus/items/'.$menu_id);
        }*/
        $this->data['item'] = $item;
        $this->data['item_id'] = $itemId;
        $items = $this->menu_model->getAllItemsWithTranslations($languageSlug);
        //var_dump($items);die();
        //$this->menu_item_translation_model->where('language_slug',$languageSlug)->order_by('title')->fields('item_id,id,title')->get_all();
        $this->data['parent_items'] = array('0' => 'Top level');
        if (!empty($items)) {
            foreach ($items as $item) {
                $this->data['parent_items'][$item->item_id] = $item->title;
            }
        }

        $this->form_validation->set_rules('parent_id', 'Parent ID', 'trim|is_natural|required');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('css-class', 'Title', 'trim');
        $this->form_validation->set_rules('url', 'Url', 'trim|required');
        $this->form_validation->set_rules('absolute_path', 'Absolute path', 'trim|is_natural');
        $this->form_validation->set_rules('order', 'Order', 'trim|is_natural|required');
        $this->form_validation->set_rules('item_id', 'item ID', 'trim|is_natural|required');
        $this->form_validation->set_rules('menu_id', 'menu ID', 'trim|is_natural_no_zero|required');
        $this->form_validation->set_rules('language_slug', 'language slug', 'trim|required');

        if ($this->form_validation->run() === false) {
            $this->render('admin/menus/items/form');
        } else {
            //var_dump($this->input->post());die();

            $loggedInOnly = $this->input->post('logged-in-only') ? $this->input->post('logged-in-only') : 0;
            $loggedOutOnly = $this->input->post('logged-out-only') ? $this->input->post('logged-out-only') : 0;
            if ( !$loggedInOnly && !$loggedOutOnly ){
                $loggedInOnly = 1;
                $loggedOutOnly = 1;
            }
            /*$both = $this->input->post('both') ? $this->input->post('both') : 0;*/
            $parent_id = $this->input->post('parent_id');
            $cssClass = $this->input->post('css-class');
            $title = $this->input->post('title');
            $url = $this->input->post('url');
            $absolute_path = $this->input->post('absolute_path');
            $order = $this->input->post('order');
            $item_id = $this->input->post('item_id');
            $menu_id = $this->input->post('menu_id');
            $language_slug = $this->input->post('language_slug');


            //this might be a fresh menu item or an exisitng one but without
            //the translation for a specific avaialble language
            $insertMenuItemTransData = array(
                'title' => $title,
                'url' => $url,
                'absolute_path' => $absolute_path == null ? '0' : '1',
                'language_slug' => $language_slug,
                'created_by' => $this->userId,
                'created_at' => date('Y-m-d H:i:s'),
            );

            if ($onlytrans) {   //this is an existing item but we do not have translation
                //for this $language_slug
                $insertMenuItemTransData['item_id'] = $originalId;
                if ($this->menu_model->insertItemMenuTrans($insertMenuItemTransData)) {
                    $this->session->set_flashdata('message', 'The item was added successfully');
                    redirect('admin/menus/items/' . $menu_id . '', 'refresh');
                }
            } else {   //completly new menu item
                $insertMenuItemData = array(
                    'menu_id' => $menu_id,
                    'parent_id' => $parent_id,
                    'order' => $order,
                    'styling' => $cssClass,
                    'created_by' => $this->userId,
                    'created_at' => date('Y-m-d H:i:s'),
                    'logged_in_only' => $loggedInOnly,
                    'logged_out_only' => $loggedOutOnly,
                    /*'both' => $both,*/
                );

                if ($id = $this->menu_model->insertItemMenu($insertMenuItemData)) {
                    //add the translation too
                    $insertMenuItemTransData['item_id'] = $id;
                    if ($this->menu_model->insertItemMenuTrans($insertMenuItemTransData)) {
                        $this->session->set_flashdata('message', 'The item was added successfully');
                        //die();
                        redirect('admin/menus/items/' . $menu_id . '', 'refresh');
                    }

                }
            }
            /*else
            {//update

                $updateMenuItemData = array( 'parent_id' => $parent_id,
                                            'order' => $order,
                                            'updated_by' => $this->ion_auth->user()->row()->id
                                        );
                if ( $this->menu_model->updateItemMenu($updateMenuItemData, $item_id) )
                {//update the translation too
                    $updateMenuItemTransData = array( 'item_id' => $item_id,
                                                    'title' => $title,
                                                    'url' => $url,
                                                    'absolute_path' => $absolute_path == null ? '0' : '1',
                                                    'language_slug' => $language_slug,
                                                    'updated_by' => $this->ion_auth->user()->row()->id
                                                    );
                }
                if ($this->menu_model->updateItemMenuTrans($updateMenuItemTransData, $item_id, $language_slug) )
                {
                    $this->session->set_flashdata('message', 'The item was edited successfully');
                    redirect('admin/menus/items/'.$menu_id.'', 'refresh');
                }
            }*/
            /*$insertData = array( 'item_id' => $item_id,
                'title' => $title,
                'url' => $url,
                'absolute_path' => $absolute_path,
                'language_slug' => $language_slug,
                'created_by' => $this->user_id,
                'created_at' => null,
                'updated_at' => null,
            );*/
            /*$insert_data = array('item_id' => $item_id, 'title' => $title, 'url' => $url, 'absolute_path' => $absolute_path, 'language_slug' => $language_slug, 'created_by'=>$this->user_id);

            if($translation_id = $this->menu_item_translation_model->insert($insert_data))
            {
                $this->postal->add('Item successfuly added.','success');
                $this->menu_item_model->update(array('parent_id'=>$parent_id, 'order'=>$order, 'styling'=>$styling, 'updated_by'=>$this->user_id),$item_id);
            }

            redirect('admin/menus/items/'.$menu_id);*//*$insert_data = array('item_id' => $item_id, 'title' => $title, 'url' => $url, 'absolute_path' => $absolute_path, 'language_slug' => $language_slug, 'created_by'=>$this->user_id);

            if($translation_id = $this->menu_item_translation_model->insert($insert_data))
            {
                $this->postal->add('Item successfuly added.','success');
                $this->menu_item_model->update(array('parent_id'=>$parent_id, 'order'=>$order, 'styling'=>$styling, 'updated_by'=>$this->user_id),$item_id);
            }

            redirect('admin/menus/items/'.$menu_id);*/

        }
    }

    public function editItem($menuId = null, $languageSlug = null, $itemId = 0)
    {
        // make sure we have a numeric id
        if (is_null($menuId) || !is_numeric($menuId) || $languageSlug == null) {
            redirect('admin/menus');
        }


        $itemId = (int)$itemId;
        $this->data['menu_id'] = $menuId;
        $languageSlug = (array_key_exists($languageSlug, $this->langs)) ? $languageSlug : $this->current_lang;

        $this->data['page_title'] = 'Edit Item in ' . $this->langs[$languageSlug]['name'] . ' ';
        $this->data['language_slug'] = $languageSlug;
        $item = $this->menu_model->getMenuItemById($itemId, $languageSlug);
        //var_dump($item);die();
        $translation = $this->menu_model->getTranslationsByItemIdAndSlug($itemId, $languageSlug);

        if ($translation === false) {
            $this->session->set_flashdata('error', 'Suck a dick!');
            redirect('admin/menus/items/' . $menuId);
        }
        //var_dump($item);die();
        $this->data['item'] = $item;
        $this->data['translation'] = $translation;
        $this->data['item_id'] = $itemId;

        $items = $this->menu_model->getAllItemsWithTranslations($languageSlug, $itemId);
        //var_dump($items);die();
        $this->data['parent_items'] = array('0' => 'Top level');
        if (!empty($items)) {
            foreach ($items as $item) {
                $this->data['parent_items'][$item->item_id] = $item->title;
            }
        }

        $this->form_validation->set_rules('parent_id', 'Parent ID', 'trim|is_natural|required');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('url', 'Url', 'trim|required');
        $this->form_validation->set_rules('css-class', 'Css class', 'trim');
        $this->form_validation->set_rules('absolute_path', 'Absolute path', 'trim|is_natural');
        $this->form_validation->set_rules('order', 'Order', 'trim|is_natural|required');
        $this->form_validation->set_rules('item_id', 'item ID', 'trim|is_natural|required');
        $this->form_validation->set_rules('menu_id', 'menu ID', 'trim|is_natural_no_zero|required');
        $this->form_validation->set_rules('language_slug', 'language slug', 'trim|required');

        if ($this->form_validation->run() === false) {
            $this->render('admin/menus/items/form');
        } else {
            $loggedInOnly = $this->input->post('logged-in-only') ? $this->input->post('logged-in-only') : 0;
            $loggedOutOnly = $this->input->post('logged-out-only') ? $this->input->post('logged-out-only') : 0;
            if ( !$loggedInOnly && !$loggedOutOnly ){
                $loggedInOnly = 1;
                $loggedOutOnly = 1;
            }
            $parent_id = $this->input->post('parent_id');
            $title = $this->input->post('title');
            $url = $this->input->post('url');
            $cssClass = $this->input->post('css-class');
            $absolute_path = $this->input->post('absolute_path');
            $order = $this->input->post('order');
            $translation_id = $this->input->post('translation_id');
            $item_id = $this->input->post('item_id');
            $menu_id = $this->input->post('menu_id');

            $updateData = array(
                'title' => $title,
                'url' => $url,
                'absolute_path' => $absolute_path,
                'updated_by' => $this->userId
            );

            if ($this->menu_model->updateItemMenuTrans($updateData, $translation_id, $languageSlug)) {
                $updateMenuItemData = array(
                    'parent_id' => $parent_id,
                    'styling' => $cssClass,
                    'order' => $order,
                    'updated_by' => $this->userId,
                    'logged_in_only' => $loggedInOnly,
                    'logged_out_only' => $loggedOutOnly,
                );
                if ($this->menu_model->updateItemMenu($updateMenuItemData, $item_id)) {
                    $this->session->set_flashdata('message', 'The item was edited successfully');
                    redirect('admin/menus/items/' . $menu_id . '', 'refresh');
                }
            }
        }
    }

    public function deleteItem($menuId = null, $languageSlug = null, $itemId)
    {
        // make sure we have a numeric id
        if (is_null($menuId) || !is_numeric($menuId) || $languageSlug == null) {
            redirect('admin/menus');
        }

        if ($languageSlug == 'all') {
            // delete all the translations for the item and the item
            $this->menu_model->deleteMenuItemTrans($itemId);
            $this->menu_model->deleteMenuItems($itemId);
            $this->session->set_flashdata('message', 'The item was successfully deleted.');
            redirect('admin/menus/items/' . $menuId . '', 'refresh');
        } else {
            //delete the translation for a specific lang
            if ($this->menu_model->deleteMenuItemTransLang($itemId, $languageSlug)) {
                $this->session->set_flashdata('message', 'The translation was successfully deleted.');
                redirect('admin/menus/items/' . $menuId . '', 'refresh');
            }
        }
    }
}