<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/16/2015
 * Time: 1:16 PM
 */

class Pages extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('pages_model');
    }

    public function index() {
        $this->data['page_title'] = 'Pages';
        $this->data['pages'] = $this->pages_model->getAll();
        $this->render('admin/pages/list_pages');
    }

    /**
     * This method creates an item for a menu or edit it for a language
     * when the itemid is 0 we are creating a new item else editing existing one
     * @param null $menuId
     * @param null $languageSlug
     * @param int $itemId
     */
    public function createPage($languageSlug = null, $pageId = 0)
    {
        // make sure we have a numeric id
        if ($languageSlug == null) {
            redirect('admin/pages');
        }

        $this->data['page_id'] = (int)$pageId;
        $languageSlug = (array_key_exists($languageSlug, $this->langs)) ? $languageSlug : $this->current_lang;

        $this->data['page_title'] = 'Add page in ' . $this->langs[$languageSlug]['name'] . ' ';
        //var_dump($this->data['content_language']);die();
        $this->data['language_slug'] = $languageSlug;
        $page = $this->pages_model->getPageById((int)$pageId, $languageSlug);
        $onlytrans = false;

        if ($page == false) {
            //$originalId = $itemId;
            $pageId = 0;
            //$onlytrans = true;
        }

        $this->data['page'] = $page;
        $this->data['page_id'] = $pageId;

        $this->form_validation->set_rules('short_title', 'Short title', 'trim');
        $this->form_validation->set_rules('title', 'Title', 'trim');
        $this->form_validation->set_rules('language_slug', 'language slug', 'trim|required');
        $this->form_validation->set_rules('content', 'Content', 'trim');
        $this->form_validation->set_rules('page_keywords', 'SEO page title', 'trim|required');
        $this->form_validation->set_rules('page_description', 'SEO page description', 'trim|required');
        $this->form_validation->set_rules('page_title', 'SEO page keywords', 'trim|required');
        $this->form_validation->set_rules('page_id', 'Page ID', 'trim|is_natural|required');
        $this->form_validation->set_rules('is_active', 'Page active', 'trim|is_natural|required');

        if ($this->form_validation->run() === false) {
            $this->render('admin/pages/form');
        } else {

            $title = $this->input->post('title');
            $pageId = $this->input->post('page_id');
            $pageActive = $this->input->post('is_active');
            $content = $this->input->post('is_active');
            $pageTitle = $this->input->post('page_title');
            $pageDescription = $this->input->post('page_description');
            $pageKeyqords = $this->input->post('page_keywords');
            $languageSlug = $this->input->post('language_slug');
            $shortTitle = $this->input->post('short_title');

            if ( !$pageId ){//this is a fresh new page
                $insertPageData = array(
                    'active' => $pageActive,
                    'created_by' => $this->userId,
                    'created_at' => date('Y-m-d H:i:s'),
                );
                //insert the new page and get the id
                if ($id = $this->pages_model->insertPage($insertPageData)) {
                    //add the translation too
                    $insertPageTransData['page_id'] = $id;

                    $pageTransData = array(
                        'page_id' => $id,
                        'title'   => $title,
                        'language_slug' => $languageSlug,
                        'short_title'   => $shortTitle,
                        'content' => $content,
                        'page_title'   => $pageTitle,
                        'page_description'   => $pageDescription,
                        'page_keywords'   => $pageKeyqords,
                        'created_by' => $this->userId,
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    if ($this->pages_model->insertPageTrans($pageTransData)) {
                        $this->session->set_flashdata('message', 'The page was added successfully');
                        //die();
                        redirect('admin/pages', 'refresh');
                    }

                }
                var_dump($insertPageData);die();
            } else {
                die("existing page");
            }



            var_dump($this->input->post());die();

            if ($onlytrans) {   //this is an existing item but we do not have translation
                //for this $language_slug
                $insertMenuItemTransData['item_id'] = $originalId;
                if ($this->menu_model->insertItemMenuTrans($insertMenuItemTransData)) {
                    $this->session->set_flashdata('message', 'The item was added successfully');
                    redirect('admin/menus/items/' . $menu_id . '', 'refresh');
                }
            } else {   //completly new menu item
                $insertMenuItemData = array(
                    'menu_id' => $menu_id,
                    'parent_id' => $parent_id,
                    'order' => $order,
                    'created_by' => $this->userId,
                    'created_at' => date('Y-m-d H:i:s'),
                );

                if ($id = $this->menu_model->insertItemMenu($insertMenuItemData)) {
                    //add the translation too
                    $insertMenuItemTransData['item_id'] = $id;
                    if ($this->menu_model->insertItemMenuTrans($insertMenuItemTransData)) {
                        $this->session->set_flashdata('message', 'The item was added successfully');
                        //die();
                        redirect('admin/menus/items/' . $menu_id . '', 'refresh');
                    }

                }
            }

        }
    }

}