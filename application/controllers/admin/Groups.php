<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 10/30/2015
 * Time: 12:57 PM
 */


class Groups extends Admin_Controller {

    function __construct() {
        parent::__construct();
        //only the users in the admin group can edit the groups
        if(!$this->ion_auth->in_group('admin')) {
            $this->session->set_flashdata('message', 'You are not allowed to visit the Groups page');
            redirect('admin', 'refresh');
        }
    }

    public function index() {
        $this->data['page_title'] = 'Groups';
        $this->data['groups'] = $this->ion_auth->groups()->result();
        $this->render('admin/groups/list_groups');
    }

    public function create() {

        $this->data['page_title'] = 'New group';
        $this->form_validation->set_rules('group_name','Group name','trim|required|is_unique[groups.name]');
        $this->form_validation->set_rules('description','Group description','trim|required');

        if($this->form_validation->run() === FALSE)
        {
            $this->render('admin/groups/form');
        }
        else
        {
            $groupName = $this->input->post('group_name');
            $groupDescription = $this->input->post('description');
            $this->ion_auth->create_group($groupName, $groupDescription);
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('admin/groups', 'refresh');
        }
    }

    public function edit($id = null) {
        // make sure we have a numeric id
        if (is_null($id) OR !is_numeric($id)) {
            redirect('admin/groups');
        }

        $groupId = $id;
        $this->data['page_title'] = 'Edit group';

        $this->form_validation->set_rules('group_name','Group name','trim|required');
        $this->form_validation->set_rules('description','Group description','trim|required');
        $this->form_validation->set_rules('group_id','Group id','trim|integer|required');

        if($this->form_validation->run() === FALSE) {
            if($group = $this->ion_auth->group((int) $groupId)->row()) {
                $this->data['group'] = $group;
            } else {
                $this->session->set_flashdata('error', 'The group doesn\'t exist.');
                redirect('admin/groups', 'refresh');
            }
            $this->render('admin/groups/form');
        } else {
            $groupName = $this->input->post('group_name');
            $groupDescription = $this->input->post('description');
            $groupId = $this->input->post('group_id');
            $this->ion_auth->update_group($groupId, $groupName, $groupDescription);
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('admin/groups', 'refresh');
        }
    }

    public function delete($id = null) {
        // make sure we have a numeric id
        if (is_null($id) OR !is_numeric($id)) {
            redirect('admin/groups');
        }

        $this->ion_auth->delete_group($id);
        $this->session->set_flashdata('message', $this->ion_auth->messages());

        redirect('admin/groups','refresh');
    }
}