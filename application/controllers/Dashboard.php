<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Private_Controller {

	private $macros = array('proteins', 'carbohydrates', 'fats', 'calories');
	/**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        //var_dump();die();
        // load the language file
        $this->lang->load('journal', $this->langs[$this->current_lang]['name']);
	
        // load the model file
        $this->load->model('dashboard_model');
        
    }
    /**
     * the default function will return all the info 
     * regarding the user macros and probably his evolution
     * @return [type]
     */
    public function index()
    {
        $this->data['page_title'] = 'Dashboard';
        //$data = $this->includes;
        //$content_data = array();
        //var_dump();die();
        if ( $macros = $this->dashboard_model->getMacrosByUserId($this->user['id'])  )
        {	//we have data for today
        
        	$content_data = $this->prepareMacrosForView($macros);
        
        }
        else
        {//no data for today so we will give some default values
        	foreach ( $this->macros as $macro ) {
        		$content_data["{$macro}"] = 1;
        	}
        }
        
        // if ( !$this->getLastSevenDaysMacros() )
        // {//there are no records for the last 7 days
        //     $content_data['SevenDaysData']['proteins'] = 1;
        //     $content_data['SevenDaysData']['carbohydrates'] = 1;
        //     $content_data['SevenDaysData']['fats'] = 1;
        //     $content_data['SevenDaysData']['calories'] = 1;
        // }
        
        $content_data['SevenDaysData'] = $this->getLastSevenDaysMacros();
        $content_data['lastSevenDays'] = $this->getLastSevenDays();
        
        $this->addJs("".site_url()."assets/front/js/journal.js");

        //var_dump($this->getLastSevenDaysMacros());die();
        // die("?");
        //var_dump($this->data);die();
        $this->data['journal_data'] = $content_data;
        $this->render('public/dashboard/index');
    	//$data['content'] = $this->load->view('dashboard/index', $content_data, true);
    	//$this->load->view('template.php', $data);
    }

    public function testing(){
        if ( $macros = $this->dashboard_model->getMacrosByUserId($this->user['id'])  )
        {   //we have data for today
        
            $content_data = $this->prepareMacrosForView($macros);
        
        }
        else
        {//no data for today so we will give some default values
            foreach ( $this->macros as $macro ) {
                $content_data["{$macro}"] = 1;
            }
        }

        die(json_encode(array('result' => true, 'message' => 'The macros have been successfully updated.', 'data' => $content_data)));
    }

     /**
     * Make sure the macro is valid
     * /^[0-9]+([.][0-9]+)?$/ - only float and int
     * @param  string $macro
     * @return int|boolean
     */
    function _check_macro($macro)
    {
        if (trim($macro) !== '' && preg_match('/^[0-9]+([.][0-9]+)?$/', $macro))
        { 
           return true;
        }
        else
        {
            $this->form_validation->set_message('_check_macro', 'the {field} must be a positive number or float');
            return false;
        }
    }
    /**
     * This function will return an array that contains all of the macros for the last 7 days
     * (proteins, carbs, fats and calories)
     */
    private function getLastSevenDaysMacros()
    {   
        $dates = $this->getLastSevenDays();
        
        if ( $allMacrs = $this->dashboard_model->getUserSevenDaysStatus($this->user['id']) )
        {   
            $proteins = array();
            $carbs = array();
            $fats = array();
            $calories = array();
            $availableDataForDates = array();
            $macros = array();
            $currentIndex = 0;
            //we are creating an array with the last 7 logged days from the DB
            foreach ( $allMacrs as $macrosConcat )
            {
                $availableDataForDates[] = $macrosConcat['created_on'];
                $macros[]['macros'] = $macrosConcat['macros'];
            }
            
            foreach ( $allMacrs as $macrosConcat )
            {   //we check if each day from the last 7 days, from today, is avaiable in the array returned from db
                //If it is we add the macros coresponding to that date from the db
                //else we just display to the user the default values
                if ( array_search($dates['dates'][$currentIndex], $availableDataForDates) !== false )
                {
                    foreach ( $macrosConcat as $string )
                    {   
                        $macros = explode( ',', $string);
                        $currentDate = $macrosConcat['created_on'];
                        
                        foreach ( $macros as $k => $v )
                        {   
                            if ( $v == 'proteins' )
                            {   
                                $proteins[] = $macros[$k+1];
                                
                            }
                            elseif ( $v == 'carbohydrates' )
                            {   
                                $carbs[] = $macros[$k+1];
                                
                            }
                            elseif ( $v == 'fats' )
                            {
                                $fats[] = $macros[$k+1];
                            }
                            elseif ( $v == 'calories' )
                            {
                                $calories[] = $macros[$k+1];
                            }
                        }
                    }
                    $currentIndex++;
                } else  {
                    $proteins[] = 1;
                    $carbs[] = 1;
                    $fats[] = 1;
                    $calories[] = 1;
                    $currentIndex++;
                }
            }
            return array('proteins' => $proteins, "carbs" => $carbs, "fats" => $fats, "calories" => $calories);
        }
        else
        {//we do not have t=data for this user
            $macros = array('proteins' => '', "carbs" => '', "fats" => '', "calories" => '');
            
            foreach ( $macros as $key => $val )
            {
                for ( $i=0; $i<7; $i++ )
                {
                    $macros[$key][] = 1;
                }
            }
            return $macros;
            //var_dump($macros);die();
        }
    }
    /**
     * return last 7 days from now in d-m-Y format
     */ 
    private function getLastSevenDays()
    {
        $now = new DateTime( "7 days ago", new DateTimeZone('Europe/Bucharest'));
        $interval = new DateInterval( 'P1D'); // 1 Day interval
        $period = new DatePeriod( $now, $interval, 7 ); // 7 Days
        $lastSevenDays = array();
        foreach( $period as $day) {
            $key = $day->format( 'Y-m-d' );
            $lastSevenDays[] = $key;
        }
        //var_dump($lastSevenDays);
        array_pop($lastSevenDays);
        
        //var_dump($lastSevenDays);die();
        return array('dates' => array_reverse($lastSevenDays)); 
    }
    /**
     * prepare an array with all the macros for view
     */ 
    private function prepareMacrosForView($macros)
    {
        $content_data = array();
    	foreach ( $macros as $macro ) {
    		switch ($macro['name']) {
    			case 'proteins':
					$content_data['proteins'] = $macro['macro_value'];
				break;
				case 'carbohydrates':
					$content_data['carbohydrates'] = $macro['macro_value'];
				break;
				case 'fats':
					$content_data['fats'] = $macro['macro_value'];
				break;
				case 'calories':
					$content_data['calories'] = $macro['macro_value'];
				break;
    			
    			default:
    			break;
    		}
    	}
    	
    	return $content_data;
    }
    /**
     * Save the current macros
     * we support ajax and normal server request
     */
    public function saveMacros()
    {	
       if (!$this->input->is_ajax_request()){
            redirect(base_url(''.$this->current_lang.'/jurnal/'));
       }
        //validations
    	// $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
     //    $this->form_validation->set_rules('proteins', lang('proteins'), 'required|callback__check_macro');
     //    $this->form_validation->set_rules('carbohydrates', lang('carbohydrates'), 'required|callback__check_macro');
     //    $this->form_validation->set_rules('fats', lang('fats'), 'required|callback__check_macro');
     //    $this->form_validation->set_rules('calories', lang('calories'), 'required|callback__check_macro');
        
        // if ($this->form_validation->run() == TRUE)
        // {
        	$postData = $this->input->post();
        	unset($postData['submit']);
        	$saveData = array();
            $i = 1;
    		foreach ( $postData as $k => $macro )
    		{
    		   	$saveData[] = array(
    		   	                    'macro_id'      => $i++,
    		   	                    'created_on'    => date("Y-m-d"), 
    		   	                    'user_id'       => $this->user['id'], 
    		   	                    'macro_value'   => $macro
    		   	                    );
    		}
    		
        	//$this->debug($saveData);
        	if ( $this->dashboard_model->saveMacros($this->user['id'], $saveData) )
        	{   
    	        $macros = $this->dashboard_model->getMacrosByUserId($this->user['id']);
                //var_dump($macros);die();
    	        //we have data for today
            	$content_data = $this->prepareMacrosForView($macros);
            	//$lastWeekStatus = $this->getLastSevenDaysMacros();
    	        die(json_encode(array('result' => true, 'message' => 'The macros have been successfully updated.', 'data' => $content_data)));
        	}
        	else
        	{
        	   die(json_encode(array('result' => false, 'message' => 'Internal error.')));
        	}
        //}

       redirect(base_url(''.$this->current_lang.'/jurnal/'));
    }
    
    public function changeLanguage()
    {
       $this->session->set_userdata($this->input->post());
       die(json_encode(array('result' => true)));
        //print_r("test");die();
    }
    
    public function calculateMacros()
    {
        $response = array();
        $postData = $this->input->post();
        $customGrams = $postData['custom_grams'];
        unset($postData['custom_grams']);
        
        
        foreach ( $postData as $macro => $value )
        {
            $response[$macro] = ($value * $customGrams) / 100;
        }
        
        die(json_encode(array('result' => true, 'message' => 'Success', 'data' => $response)));
        //print_r($response);die();
    }
}