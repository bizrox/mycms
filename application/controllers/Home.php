<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Public_Controller {

	/**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
    }

    public function index(){
        $this->data['page_title'] = 'Homepage';
        //$this->addCss("".site_url()."front/assets/css/test.css");
        $this->render('public/home/index');
    }
}