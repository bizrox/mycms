<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$controllersMethodsLanguageMappings = array(
    'en' => array(
        'welcome/list' => 'welcome/list',
        'welcome' => 'welcome',
        'auth/register'   => 'auth/register',
        'auth/login'   => 'auth/login',
    ),
    'ro' => array(
        'autentificare/login'   => 'auth/login',
        'autentificare/logout'   => 'auth/logout',
        'autentificare/profil'   => 'auth/edit_user',
        'autentificare/resetare-parola'   => 'auth/forgot_password',
        'autentificare/cont-nou'   => 'auth/register',
        'jurnal'   => 'dashboard/index',
        'jurnal/saveMacros'   => 'dashboard/saveMacros',
        'jurnal/testing'   => 'dashboard/testing',
    )
);

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'admin/dashboard';
/**
 * That means that when someone changes language, the application won�t start to look for a controller that has two letters and then output a 404.
 * With this route, the application will know that whatever link has only two letters, it will be directed to the default controller (Welcome.php).
 * The Welcome.php, extending the MY_Controller, will know what language is asked for and output the content accordingly.
 */
$route['^(\w{2})/(.*)$'] =  function($language, $link) use ($controllersMethodsLanguageMappings)
{//var_dump($link);die();
    foreach($controllersMethodsLanguageMappings[$language] as $key => $sym_link)
    {   

        if ( strtolower($link) === strtolower($key) )
        {   
            $newLink = $sym_link;
            //die($newLink);
            return $newLink;
        }

    }

    show_404();
};
$route['^(\w{2})$'] = $route['default_controller'];
