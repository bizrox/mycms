<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 10/30/2015
 * Time: 11:17 AM
 */
class MY_Controller extends CI_Controller {

    protected $data = array();
    /**
     * @var array("en") = array('id' => $lang->id,
    *                                'slug' => $lang->slug,
     *                               'name' => $lang->language_name,
      *                              'language_directory' => $lang->language_directory,
      *                              'language_code' => $lang->language_code,
      *                              'default' => $lang->default)
     *
     */
    protected $langs = array();
    /**
     * en, ro etc.
     */
    protected $default_code_lang;
    /**
     * default current language: en, ro etc.
     */
    protected $current_lang;
    /**
     * ex: english, romanian
     */
    protected $default_full_lang;

    function __construct() {
        parent::__construct();
        $settings = $this->settings_model->get_settings();
        $this->settings = new stdClass();
        foreach ($settings as $setting)
        {
            $this->settings->{$setting['name']} = $setting['value'];
        }
        // set the time zone
        $timezones = $this->config->item('timezones');
        //var_dump($timezones);die();
        date_default_timezone_set($timezones[$this->settings->timezones]);
        //default page title
        $this->data['page_title'] = 'myCMS';
        //settings
        $this->data['settings'] = $this->settings;
        //this will contain all the data we want to output
        //before closing the head section of the html(meta tags etc.)
        $this->data['before_head'] = '';
        //same as above(js files etc.)
        $this->data['before_body'] ='';

        $this->load->model('language_model');
        $availableLanguages = $this->language_model->getAll();
        if(isset($availableLanguages))
        {
            foreach($availableLanguages as $lang)
            {
                $this->langs[$lang->slug] = array('id' => $lang->id,
                                                  'slug' => $lang->slug,
                                                   'name' => $lang->language_name,
                                                  'language_directory' => $lang->language_directory,
                                                  'language_code' => $lang->language_code,
                                                   'default' => $lang->default);
                if($lang->default == '1') $this->default_code_lang = $lang->slug;
            }
        }

        // Verify if we have a language set in the URL;
        $langSlug = $this->uri->segment(1);
        $array = array();
        // If we do, and we have that languages in our set of languages we store the language slug in the session
        if(isset($langSlug) && array_key_exists($langSlug, $this->langs))
        {
            $this->current_lang = $langSlug;
            $array['set_language'] = $langSlug;
            $this->session->set_userdata($array);
            //$_SESSION['set_language'] = $langSlug;
        }
        // If not, we set the language session to the default language
        else
        {
            $this->current_lang = $this->default_code_lang;
            $array['set_language'] = $this->default_code_lang;
            $this->session->set_userdata($array);
            //$_SESSION['set_language'] = $this->default_lang;
        }
        $this->default_full_lang = strtolower($this->langs[$this->current_lang]['language_directory']);
        // Now we store the languages as a $data key, just in case we need them in our views
        $this->data['langs'] = $this->langs;
        // Also let's have our current language in a $data key
        $this->data['current_lang'] = $this->langs[$this->current_lang];

        // For links inside our views we only need the lang slug. If the current language is the default language we don't need to append the language slug to our links
        if( $this->current_lang != $this->default_code_lang )
        {
            $this->data['lang_slug'] = $this->current_lang.'/';
        }
        else
        {
            $this->data['lang_slug'] = '';
        }


        //var_dump( $this->session);die();
    }

    /**
     * @param null $viewFilePath
     * @param string $template
     */
    protected function render($viewFilePath = NULL, $template = 'master') {
        if($template == 'json' || $this->input->is_ajax_request()) {
            header('Content-Type: application/json');
            echo json_encode($this->data);
        } else {
            $this->data['the_view_content'] = (is_null($viewFilePath)) ? '' : $this->load->view($viewFilePath, $this->data, TRUE);;
            $this->load->view('templates/'.$template.'', $this->data);
        }
    }

    /**
     * @param $cssFiles
     * @param null $path
     * @return $this
     * this method will load the css files.
     * With this method we can load specific page css
     */
    function addcss( $cssFiles, $path = NULL ) {
        // if $css_files is string, then convert into array
        $cssFiles = is_array( $cssFiles ) ? $cssFiles : explode( ",", $cssFiles );

        foreach( $cssFiles as $css )
        {
            // remove white space if any
            $css = trim( $css );

            // go to next when passing empty space
            if ( empty( $css ) ) continue;

            // using sha1( $css ) as a key to prevent duplicate css to be included
            $this->data['css_files'][sha1($css)] = is_null( $path ) ? $css : $path . $css;
        }

        return $this;
    }

    /**
     * @param $jsFiles
     * @param null $path
     * @return $this
     * same shit here
     */
    function addJs( $jsFiles, $path = NULL ){

        // if $js_files is string, then convert into array
        $jsFiles = is_array( $jsFiles ) ? $jsFiles : explode( ",", $jsFiles );

        foreach( $jsFiles as $js )
        {
            // remove white space if any
            $js = trim( $js );

            // go to next when passing empty space
            if ( empty( $js ) ) continue;

            // using sha1( $css ) as a key to prevent duplicate css to be included
            $this->data[ 'js_files' ][ sha1( $js ) ] = is_null( $path ) ? $js : $path . $js;
        }

        return $this;
    }
}

class Admin_Controller extends MY_Controller {

    protected $userId = '';

    function __construct() {
        parent::__construct();

        $this->data['page_title'] = 'myCMS - Dashboard';
        //verify if the user is logged in
        if (!$this->ion_auth->logged_in() ) {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        $currentUser = $this->ion_auth->user()->row();
        $this->data['current_user'] = $currentUser;
        $this->userId = $currentUser->id;
        $this->data['current_user_menu'] = '';
        //we will show different menu items, depends on the group the user is in
        if($this->ion_auth->in_group('admin')) {
            $this->data['current_user_menu'] = $this->load->view('templates/partials/menu_admin.php', NULL, TRUE);
        } else {
            redirect('/');
        }
    }

    protected function render($viewFilePath = NULL, $template = 'admin') {
        parent::render($viewFilePath, $template);
    }
}

class Public_Controller extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('menus');
        //load common css
        $this->addCss(
            array(
                //"//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css",
                /*//"//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css",*/
                "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css",
                "//cdn.jsdelivr.net/alertifyjs/1.4.1/css/alertify.min.css",
                "//cdn.jsdelivr.net/alertifyjs/1.4.1/css/themes/default.min.css",
                "".site_url()."/assets/front/css/bootflat.css",
                "".site_url()."/assets/front/css/register.css",
                /*"".site_url()."assets/front/css/application.css",*/

                //"".site_url()."assets/front/css/main.css",
                
                /*"".site_url()."assets/front/css/animations.css",
                "".site_url()."assets/front/css/application.css",
                "".site_url()."assets/front/css/core.css",*/
                "".site_url()."/assets/front/css/styles.css",
                "".site_url()."/assets/front/css/bootstrap2.css",
                "".site_url()."/assets/front/css/formValidation.css",
                "".site_url()."/assets/front/css/responsive.css",
                //"".site_url()."assets/front/css/owl.carousel.css",
                "".site_url()."assets/front/css/jquery.bxslider.css",
                "".site_url()."assets/front/css/styles.css",
                "".site_url()."assets/front/css/fontello.css",
                "".site_url()."/assets/front/css/style.css",
                "".site_url()."/assets/front/css/parsley.css",
                /* "".site_url()."assets/front/css/theme.css",
                 "".site_url()."assets/front/css/language.css",*/
            ));
        //load common js
        $this->addJs(
            array(
                "//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js",
                //"//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js",
                "//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js",
                "//cdn.jsdelivr.net/alertifyjs/1.4.1/alertify.min.js",
                "".site_url()."/assets/front/js/custom.js",
                "".site_url()."/assets/front/js/responsive-nav.min.js",
                "".site_url()."/assets/front/js/plugins/owl.carousel.min.js",
                "".site_url()."/assets/front/js/global.js",
                "".site_url()."/assets/front/js/plugins/jquery.bxslider.min.js",
                "".site_url()."assets/front/js/plugins/Chart.min.js",
                "".site_url()."assets/front/js/plugins/parsley_validation/parsley.min.js",
                "".site_url()."assets/front/js/plugins/parsley_validation/ro.js",
                /*"".site_url()."assets/front/js/main.js",
                "".site_url()."assets/front/js/plugins.js",
                "".site_url()."assets/front/js/global.js",
                "".site_url()."assets/front/js/icheck.min.js",
                "".site_url()."assets/front/js/jquery.fs.selecter.min.js",
                "".site_url()."assets/front/js/jquery.fs.stepper.min.js",
                "".site_url()."assets/front/js/plugins/jquery.sticky.js",
                "".site_url()."assets/front/js/plugins/bootstrap.min.js",
                "".site_url()."assets/front/js/plugins/jquery.nicescroll.min.js",
                "".site_url()."assets/front/js/plugins/calculator.js",
                "".site_url()."assets/front/js/plugins/jquery.localscroll-min.js",
                "".site_url()."assets/front/js/plugins/jquery.scrollTo-min.js",
                "".site_url()."assets/front/js/plugins/jquery.sticky.js",
                "".site_url()."assets/front/js/plugins/superfish.js",
                "".site_url()."assets/front/js/plugins/jquery.ui.totop.js",
                "".site_url()."assets/front/js/plugins/jquery.easing.1.3.js",
                "".site_url()."assets/front/js/plugins/jquery.tooltipster.min.js",*/
            ));

        //verify if the user is logged in
        /*if (!$this->ion_auth->logged_in() ) {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        $currentUser = $this->ion_auth->user()->row();
        $this->data['current_user'] = $currentUser;*/
       //var_dump($_SESSION);die();
       //var_dump($this->session->has_userdata('user_info'));die();
    }

    protected function render($viewFilePath = NULL, $template = 'public') {
        //get the menu
        if ($this->ion_auth->logged_in() ) {
            $this->data['top_menu'] = $this->menus->getMenu('topmenu', $this->current_lang , 'bootstrap_menu', $loggedIn = true);
            //redirect them to the login page
            //redirect('auth/login', 'refresh');
        } else {
            $this->data['top_menu'] = $this->menus->getMenu('topmenu', $this->current_lang , 'bootstrap_menu' , $loggedIn = false);
        }

        //var_dump($this->data['top_menu']);die();
        parent::render($viewFilePath, $template);
    }
}

class Private_Controller extends Public_Controller {

    protected $user = array();

    function __construct() {
        parent::__construct();

        if ( !$this->ion_auth->logged_in() ) {
            redirect('/');
        }
        
        $this->user = (array)$this->session->userdata('user_info');
        //var_dump($this->user);die();
    }


}