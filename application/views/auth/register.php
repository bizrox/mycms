<div class="row bs-wizard" style="border-bottom:0;">
    <!--<div class="container">-->
        <div class="col-xs-3 bs-wizard-step active 1">
          <div class="text-center bs-wizard-stepnum ">Step 1</div>
          <div class="progress"><div class="progress-bar"></div></div>
          <a href="#" class="bs-wizard-dot"></a>
          <div class="bs-wizard-info text-center">Profile Information</div>
        </div>
        
        <div class="col-xs-3 bs-wizard-step 2"><!-- complete -->
          <div class="text-center bs-wizard-stepnum ">Step 2</div>
          <div class="progress"><div class="progress-bar"></div></div>
          <a href="#" class="bs-wizard-dot"></a>
          <div class="bs-wizard-info text-center">Your body data</div>
        </div>
        
        <div class="col-xs-3 bs-wizard-step 3"><!-- complete -->
          <div class="text-center bs-wizard-stepnum ">Step 3</div>
          <div class="progress"><div class="progress-bar"></div></div>
          <a href="#" class="bs-wizard-dot"></a>
          <div class="bs-wizard-info text-center">Your goal</div>
        </div>
        
        <div class="col-xs-3 bs-wizard-step"><!-- active -->
          <div class="text-center bs-wizard-stepnum step-4">Step 4</div>
          <div class="progress"><div class="progress-bar"></div></div>
          <a href="#" class="bs-wizard-dot"></a>
          <div class="bs-wizard-info text-center">Done</div>
        </div>
    <!--</div>-->
</div>    
<div class="row">
    <div class="col-sm-6 col-sm-offset-3 form-box">
    	<form role="form" action="" method="post" class="registration-form" id="register">
    		<fieldset>
            	<div class="form-top">
            		<div class="form-top-left">
            			<h3>Step 1 / 3</h3>
                		<p>Profile information</p>
            		</div>
            		<div class="form-top-right">
            			<i class="fa fa-user"></i>
            		</div>
                </div>
                <div class="form-bottom">
                	<div class="form-group">
                	    <div class="input-group input-group-md">
                	        <span class="input-group-addon"><i class="rt-icon-user2"></i></span>
                    	    <input type="text" name="first_name" placeholder="First name..." class="form-first-name form-control" id="form-first-name" value="<?php echo set_value('first_name'); ?>">
                	    </div>
                    </div>
                    <div class="form-group">
                	    <div class="input-group input-group-md">
                            <span class="input-group-addon"><i class="rt-icon-user2"></i></span>
                        	<input type="text" name="last_name" placeholder="Last name..." class="form-last-name form-control" id="form-last-name" value="<?php echo set_value('last_name'); ?>">
                    	</div>
                    </div>
                    <div class="form-group">
                	    <div class="input-group input-group-md">
                        <span class="input-group-addon"><i class="rt-icon-email"></i></span>
                    	<input type="text" name="email" placeholder="Email..." class="form-last-name form-control" id="form-email" value="<?php echo set_value('email'); ?>">
                    	</div>
                    </div>
                    <div class="form-group">
                	    <div class="input-group input-group-md">
                    	<span class="input-group-addon"><i class="rt-icon-user-outline"></i></span>
                    	<input type="text" name="username" placeholder="Username..." class="form-last-name form-control" id="form-username" 
                    	maxlength="20"
                        minlength="3"
                        data-fv-stringlength-message="The username must be more than 3 and less than 20 characters" value="<?php echo set_value('username'); ?>">
                    </div>
                    </div>
                    <div class="form-group">
                	    <div class="input-group input-group-md">
                    	<span class="input-group-addon"><i class="rt-icon-lock"></i></span>
                    	<input type="password" name="password" placeholder="Password..." class="form-last-name form-control" id="form-password" >
                    </div>
                    </div>
                     <div class="form-group">
                	    <div class="input-group input-group-md">
                    	<span class="input-group-addon"><i class="rt-icon-lock"></i></span>
                    	<input type="password" name="password_repeat" placeholder="Repeat password" class="form-last-name form-control" id="form-password_repeat">
                    </div>
                     </div>
                     <div class="modal-footer">
                         <input type="submit" class="btn btn-success btn-next" value="Next" />
                     </div>
                    
                </div>
            </fieldset>
            
            <fieldset class="plans">
            	<div class="form-top">
            		<div class="form-top-left">
            			<h3>Step 2 / 3</h3>
                		<p>Set up your goals:</p>
            		</div>
            		<div class="form-top-right">
            			<i class="fa fa-key"></i>
            		</div>
                </div>
                <div class="form-bottom form-horizontal">
                    <div class="form-group row">
                        <label class="col-xs-5 control-label">Choose a measuring unit: </label>
                        <div class="col-xs-7">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default unit">
                                <input type="radio" name="unit" value="metric"  value="<?php echo set_value('metric'); ?>"/> Metric
                            </label>
                            <label class="btn btn-default unit">
                                <input type="radio" name="unit" value="imperial" value="<?php echo set_value('imperial'); ?>" /> Imperial
                            </label>
                        </div>
                    </div>
                </div>
                <div class="wrapper" style="display: none;">
                    <div class="form-group">
                        <label class="col-xs-5 control-label">Gender: </label>
                        <div class="col-xs-7">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default">
                                    <input type="radio" name="gender" value="male" <?php echo set_radio('gender', 'male'); ?>/> Male
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="gender" value="female" <?php echo set_radio('gender', 'female'); ?> /> Female
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group birthday">
                        <label class="col-xs-5 control-label">Birthday: </label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control metric-birthday metric" name="birthday_metric" placeholder="DD/MM/YYYY" value="<?php echo set_value('birthday_metric'); ?>"/>
                            <input type="text" class="form-control imperial-birthday imperial" name="birthday_imperial" placeholder="MM/DD/YYYY" value="<?php echo set_value('birthday_imperial'); ?>"/>
                        </div>
                        <div class="col-xs-2">
                            <span class="tooltipster-icon dob-tooltip tooltip">?</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-5 control-label">Weight: </label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control imperial" name="weight" placeholder="lbs" value="<?php echo set_value('weight'); ?>" />
                            <input type="text" class="form-control metric" name="weight" placeholder="kgs"  value="<?php echo set_value('weight'); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-5 control-label">Height: </label>
                        <div class="col-xs-4">
                            <input type="text" class="form-control imperial imperial-height-ft" name="height_ft" placeholder="ft" value="<?php echo set_value('height_ft'); ?>"/>
                            <input type="text" class="form-control metric" name="height_cm" placeholder="cm" value="<?php echo set_value('height_cm'); ?>"/>
                        </div>
                        <div class="col-xs-2 imperial">
                            <input type="text" class="form-control imperial-height-in" name="height_in" placeholder="in" value="<?php echo set_value('height_in'); ?>"/>
                        </div>
                        <div class="col-xs-1">
                            <span class="tooltipster-icon tooltip height-tooltip" >?</span>
                        </div>
                    </div>
                    <div class="form-group activity-level plans">
                        <label class="col-xs-5 control-label">Your Activity Level: </label>
                        <div class="col-xs-7">
                           <div class="radio vcenter">
                                <label  class="col-xs-8"><input type="radio" name="activity_level" value="1.2" /> Sedentary</label>
                                <div class="col-xs-4">
                                    <span class="tooltipster-icon tooltip" title="little or no exercise">?</span>
                                </div>
                                <label  class="col-xs-8"><input type="radio" name="activity_level" value="1.475" /> Lightly Active</label>
                                <div class="col-xs-4">
                                    <span class="tooltipster-icon tooltip" title="light exercise or sports 1-3 days per week" >?</span>
                                </div>
                                <label  class="col-xs-8"><input type="radio" name="activity_level" value="1.55" /> Moderately active</label>
                                <div class="col-xs-4">
                                    <span class="tooltipster-icon tooltip" title="moderate exercise or sports 3-5 days per week">?</span>
                                </div>
                                <label  class="col-xs-8"><input type="radio" name="activity_level" value="1.745" /> Very active</label>
                                <div class="col-xs-4">
                                    <span class="tooltipster-icon tooltip" title="hard exercise or sports 6-7 days per week">?</span>
                                </div>
                                <label  class="col-xs-8"><input type="radio" name="activity_level" value="1.9" /> Extremely active</label>
                                <div class="col-xs-4">
                                    <span class="tooltipster-icon tooltip" title="very hard exercise or sports and a physical job or 2x training">?</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-danger btn-previous" value="Previous"/>
                        <input type="submit" class="btn btn-success btn-next " value="Next"/>
                    </div>
                    
                </div>
                
            </fieldset>
            
            <fieldset>
            	<div class="form-top">
            		<div class="form-top-left">
            			<h3>Step 3 / 3</h3>
                		<p>Social media profiles:</p>
            		</div>
            		<div class="form-top-right">
            			<i class="fa fa-twitter"></i>
            		</div>
                </div>
                <div class="form-bottom">
                	<div class="form-group">
                		<label class="sr-only" for="form-facebook">Facebook</label>
                    	<input type="text" name="form-facebook" placeholder="Facebook..." class="form-facebook form-control" id="form-facebook">
                    </div>
                    <div class="form-group">
                    	<label class="sr-only" for="form-twitter">Twitter</label>
                    	<input type="text" name="form-twitter" placeholder="Twitter..." class="form-twitter form-control" id="form-twitter">
                    </div>
                    <div class="form-group">
                    	<label class="sr-only" for="form-google-plus">Google plus</label>
                    	<input type="text" name="form-google-plus" placeholder="Google plus..." class="form-google-plus form-control" id="form-google-plus">
                    </div>
                    <button type="button" class="btn btn-previous">Previous</button>
                    <input type="submit" class="btn final-step" value="Sign up!!!"/>
                </div>
            </fieldset>
        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
        </form>
        
    </div>
</div>