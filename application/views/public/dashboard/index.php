<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 08.02.2016
 * Time: 20:17
 */
?>

<div class="row-fluid" style="margin-bottom: 25px">
    <div class="span16">
        <h1><?php echo lang('journal_heading'); ?></h1>
        <hr/>
    </div>
</div>

<div class="row-fluid">
    <div class="span4">
     <!-- <div class="tabs">
    <input type="radio" name="tab" id="tab1" checked="checked">
    <label for="tab1"><span style="font-size: 18px; margin-left: 30px;"><i style="font-size: 1.8em; position: absolute; left: -2px;" class="icon-restaurant44"></i> <?php echo lang('nutrition'); ?></span></label>
    <input type="radio" name="tab" id="tab2">
    <label for="tab2">Portfolio</label>
    <input type="radio" name="tab" id="tab3">
    <label for="tab3">Archives</label>
    <input type="radio" name="tab" id="tab4">
    <label for="tab4">Contact</label>
  
    <div class="tab-content-wrapper">
      <div id="tab-content-1" class="tab-content">
        
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur mattis nibh, non ornare neque. In bibendum consequat imperdiet. Duis eros ex, vestibulum vel fermentum ut, gravida at turpis. Etiam porta sem dolor, at finibus metus consequat a. Aliquam erat volutpat. Donec sollicitudin metus quis magna faucibus, vitae ultrices libero ultrices. Sed ut dui vitae velit laoreet commodo. Nam suscipit purus a ultricies auctor. </p>
      </div>
      <div id="tab-content-2" class="tab-content">
        
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur mattis nibh, non ornare neque. In bibendum consequat imperdiet. Duis eros ex, vestibulum vel fermentum ut, gravida at turpis. Etiam porta sem dolor, at finibus metus consequat a. Aliquam erat volutpat.  </p>
      </div>
      <div id="tab-content-3" class="tab-content">
        
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur mattis nibh, non ornare neque. In bibendum consequat imperdiet. Duis eros ex, vestibulum vel fermentum ut, gravida at turpis. Etiam porta sem dolor, at finibus metus consequat a. Aliquam erat volutpat. Donec sollicitudin metus quis magna faucibus, vitae ultrices libero ultrices. Sed ut dui vitae velit laoreet commodo. Nam suscipit purus a ultricies auctor. </p>
      </div>
      <div id="tab-content-4" class="tab-content">
        
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur mattis nibh, non ornare neque. In bibendum consequat imperdiet. Duis eros ex, vestibulum vel fermentum ut, gravida at turpis. Etiam porta sem dolor, at finibus metus consequat a. Aliquam erat volutpat. </p>
      </div>
    </div>
  </div> -->

<div class="menu-container">
      
  <div class="nav">
      <div class="fa fa-cogs fa-2x" style="float: right; margin: 10px;"></div>
  </div>
  
  <div class="menu">
      <ul>
        <li><a href="#" target="_blank"><div><i class="icon-restaurant44" style="font-size: 2.0em; position: absolute; left: -2px;"></i><span style="margin-left:50px"><?php echo lang('nutrition'); ?></span></div></a></li>
        <li><a href="#" target="_blank"><div><i class="fa fa-line-chart" style="font-size: 1.7em; position: absolute; left: 12px;"></i><span style="margin-left:50px"><?php echo lang('evolution'); ?></span></div></a></li>
        <li><a href="#" target="_blank"><div><i class="icon-dumbbells10" style="font-size: 2.0em; position: absolute; left: -2px;"></i><span style="margin-left:50px"><?php echo lang('gym_plans'); ?></span></div></a></li>
        <!-- <li><a href="#" target="_blank"><div><i class="icon-restaurant44" style="font-size: 2.0em; position: absolute; left: -2px;"></i><span style="margin-left:50px"><?php echo lang('nutrition'); ?></span></div></a></li> -->
    </div>
</div>
</div>
<div class="span12">
  <div class="md-card">
    <div class="md-card-toolbar">
        <h3 class="md-card-toolbar-heading-text">
            <?php echo lang('todays_macro'); ?>
        </h3>
    </div>
    <?php //var_dump($journal_data); ?>
    <div class="md-card-content row-fluid">
        <div class="span7">
          <canvas id="skills" width="200" height="200"></canvas>
        </div>     
        <div class="span8">
              <?php echo form_open('Dashboard/saveMacros', array('role'=>'form', 'id' => 'macros', 'class' => 'data-parsley-validate')); ?>
        <div class="row-fluid">
          <div class="form-group span12<?php echo form_error('proteins') ? ' has-error' : ''; ?>">
               <?php echo form_label(lang('proteins'), 'proteins', array('class'=>'control-label')); ?>
               <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="arrow-icon-down-bold"></i></span>
                  <?php echo form_input(array('data-parsley-trigger'=>'change', 'data-parsley-required' => 'true', 'data-parsley-pattern' => "/^[0-9]+([.][0-9]+)?$/", 'id'=> 'proteins', 'name'=>'proteins', 'value' => set_value('proteins', $journal_data['proteins']), 'class'=>'form-control')); ?>
                </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="form-group span12<?php echo form_error('carbohydrates') ? ' has-error' : ''; ?>">
               <?php echo form_label(lang('carbohydrates'), 'name', array('class'=>'control-label')); ?>
              <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                  <?php echo form_input(array('data-parsley-trigger'=>'change', 'data-parsley-required' => 'true', 'data-parsley-pattern' => "/^[0-9]+([.][0-9]+)?$/",'id'=> 'carbohydrates', 'name'=>'carbohydrates', 'value'=>set_value('carbohydrates', $journal_data['carbohydrates']), 'class'=>'form-control')); ?>
              </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="form-group span12<?php echo form_error('fats') ? ' has-error' : ''; ?>">
              <?php echo form_label(lang('fats'), 'name', array('class'=>'control-label')); ?>
              <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                  <?php echo form_input(array('data-parsley-trigger'=>'change', 'data-parsley-required' => 'true', 'data-parsley-pattern' => "/^[0-9]+([.][0-9]+)?$/",'id'=> 'fats', 'name'=>'fats', 'value'=>set_value('fats', $journal_data['fats']), 'class'=>'form-control')); ?>
              </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="form-group span12<?php echo form_error('calories') ? ' has-error' : ''; ?>">
              <?php echo form_label(lang('calories'), 'name', array('class'=>'control-label')); ?>
              <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                  <?php echo form_input(array('data-parsley-trigger'=>'change', 'data-parsley-required' => 'true', 'data-parsley-pattern' => "/^[0-9]+([.][0-9]+)?$/",'id'=> 'calories', 'name'=>'calories', 'value'=>set_value('calories',  $journal_data['calories']), 'class'=>'form-control')); ?>
              </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="form-group span12 text-center">
              <button type="submit" name="submit-macros" class="btn btn-success"><span class="glyphicon glyphicon-envelope"></span> Save</button>
          </div>
      </div>
      
      <?php echo form_close(); ?>
        </div>                      
      </div>
</div>
</div>
<?php $this->load->view('public/dashboard/partials/macros_pie'); ?>
