
<div class="row">
	<div class="col-xs-12 col-lg-7">
	    <div class="panel panel-primary">
	        <div class="panel-heading">
                <h3 class="panel-title">Today's macros</h3>
            </div>
          <div class="panel-body">
            
		<div class="col-xs-6">
			<canvas id="skills" width="300" height="300"></canvas>
		</div>
    <?php echo form_open('Dashboard/saveMacros', array('role'=>'form', 'id' => 'macros')); ?>
    
  		<div class="col-xs-5" style="margin-left: 10px;">
  			<div class="row">
          <div class="form-group col-sm-12<?php echo form_error('proteins') ? ' has-error' : ''; ?>">
               <?php echo form_label(lang('proteins'), 'proteins', array('class'=>'control-label')); ?>
               <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="arrow-icon-down-bold"></i></span>
                  <?php echo form_input(array('id'=> 'proteins', 'name'=>'proteins', 'value' => set_value('proteins', $proteins), 'class'=>'form-control')); ?>
                </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-12<?php echo form_error('carbohydrates') ? ' has-error' : ''; ?>">
               <?php echo form_label(lang('carbohydrates'), 'name', array('class'=>'control-label')); ?>
              <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                  <?php echo form_input(array('id'=> 'carbohydrates', 'name'=>'carbohydrates', 'value'=>set_value('carbohydrates', $carbohydrates), 'class'=>'form-control')); ?>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-12<?php echo form_error('fats') ? ' has-error' : ''; ?>">
              <?php echo form_label(lang('fats'), 'name', array('class'=>'control-label')); ?>
              <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                  <?php echo form_input(array('id'=> 'fats', 'name'=>'fats', 'value'=>set_value('fats', $fats), 'class'=>'form-control')); ?>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-12<?php echo form_error('calories') ? ' has-error' : ''; ?>">
              <?php echo form_label(lang('calories'), 'name', array('class'=>'control-label')); ?>
              <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                  <?php echo form_input(array('id'=> 'calories', 'name'=>'calories', 'value'=>set_value('calories', $calories), 'class'=>'form-control')); ?>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-12 text-center">
              <button type="submit" name="submit-macros" class="btn btn-success"><span class="glyphicon glyphicon-envelope"></span> Save</button>
          </div>
      </div>
  		</div>
  		
      <?php echo form_close(); ?>
	</div>
	</div>
	</div>
  <?php //$this->load->view('partials/calculator'); ?>
    <div class="col-xs-12 col-lg-5">
        <div class="panel panel-primary">
    	    <div class="panel-heading">
                <h3 class="panel-title">Calculate macros for a specific weight</h3>
            </div>
            <div class="panel-body">
                <p class="help-block">Please enter values for 100g</p>
                <div class="col-xs-12 col-lg-12">
                    <?php echo form_open('Dashboard/calculateMacros', array('role'=>'form', 'id' => 'calc-macros')); ?>
    
  		<div class="col-xs-8" style="margin-left: 10px;">
  			<div class="row">
          <div class="form-group col-sm-12<?php echo form_error('customProteins') ? ' has-error' : ''; ?>">
               <?php echo form_label(lang('proteins'), 'proteins', array('class'=>'control-label')); ?>
               <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="arrow-icon-down-bold"></i></span>
                  <?php echo form_input(array('id'=> 'custom-proteins', 'name'=>'custom_proteins', 'value' => set_value('customProteins', isset($customProteins) ? $customProteins : ''), 'class'=>'form-control')); ?>
                </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-12<?php echo form_error('customCarbohydrates') ? ' has-error' : ''; ?>">
               <?php echo form_label(lang('carbohydrates'), 'name', array('class'=>'control-label')); ?>
              <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                  <?php echo form_input(array('id'=> 'custom-carbohydrates', 'name'=>'custom_carbohydrates', 'value'=>set_value('customCarbohydrates',isset($customCarbohydrates) ? $customCarbohydrates : ''), 'class'=>'form-control')); ?>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-12<?php echo form_error('customFats') ? ' has-error' : ''; ?>">
              <?php echo form_label(lang('fats'), 'name', array('class'=>'control-label')); ?>
              <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                  <?php echo form_input(array('id'=> 'custom-fats', 'name'=>'custom_fats', 'value'=>set_value('customFats',isset($customFats) ? $customFats : ''), 'class'=>'form-control')); ?>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-12<?php echo form_error('customCalories') ? ' has-error' : ''; ?>">
              <?php echo form_label(lang('calories'), 'name', array('class'=>'control-label')); ?>
              <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                  <?php echo form_input(array('id'=> 'custom-calories', 'name'=>'custom_calories', 'value'=>set_value('customCalories', isset($customCalories) ? $customCalories : ''), 'class'=>'form-control')); ?>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-12<?php echo form_error('customGrams') ? ' has-error' : ''; ?>">
              <label for="custom-grams" class="form-label">Enter the grams you ate</label>
              <div class="input-group input-group-md">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                  <?php echo form_input(array('id'=> 'custom-grams', 'name'=>'custom_grams', 'value'=>set_value('customGrams', isset($customGrams) ? $customGrams : ''), 'class'=>'form-control')); ?>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-12 text-center">
              <button type="submit" name="submit-custom-macros" class="btn btn-success"><span class="glyphicon glyphicon-envelope"></span> Send</button>
          </div>
      </div>
  		</div>
  		
      <?php echo form_close(); ?>
                </div>
            </div>
        </div>	
    </div>
</div>
<div class="row">
    
    <div class="col-xs-12">
        <div class="panel panel-primary">
    	    <div class="panel-heading">
                <h3 class="panel-title">Last 7 days macros</h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-lg-12">
                    <canvas id="myProgress" class="col-xs-12 col-lg-12" ></canvas>
                </div>
            </div>
        </div>	
    </div>
</div>
    <?php $this->load->view('partials/charts.php'); ?>