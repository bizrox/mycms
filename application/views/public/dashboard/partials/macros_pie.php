<script type="text/javascript">

    var pieData  = [
       {
          value: <?php echo $journal_data['proteins']; ?>,
          label: 'Proteins',
          color: '#811BD6'
       },
       {
          value: <?php echo $journal_data['carbohydrates']; ?>,
          label: 'Carbs',
          color: '#9CBABA'
       },
       {
          value: <?php echo $journal_data['fats']; ?>,
          label: 'Fats',
          color: '#D18177'
       }
    ]
    var context = document.getElementById('skills').getContext('2d');
    var skillsChart = new Chart(context).Pie(pieData);
</script>