<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/4/2015
 * Time: 1:14 PM
 */
?>
<?php echo form_open('', array('role'=>'form', 'class' => 'form-horizontal')); ?>
<?php if (isset($menu->id) ) : ?>
    <?php echo form_hidden('menu_id', $menu->id); ?>
<?php endif; ?>
<div class="row">
    <h1><i class="fa fa-bars"></i> <?php echo $page_title; ?></h1>
    <hr/>
    <fieldset>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="title">title </label>
            <div class="col-md-4 <?php echo form_error('title') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'title',
                    'value'=>set_value('title', (isset($menu->title) ? $menu->title : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Title')); ?>
            </div>
        </div>
    </fieldset>
    <!-- Button (Double) -->
    <div class="form-group" style="padding-top: 20px;">
        <label class="col-md-4 control-label" for="submit_menu"></label>
        <div class="col-md-8">
            <button id="submit_group" name="submit_menu" class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
