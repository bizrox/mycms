<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/2/2015
 * Time: 4:43 PM
 */
?>

<?php if ($this->session->flashdata('errors')) : ?>
    <?php foreach ( $this->session->flashdata('errors') as $error ): ?>
        <div class="row alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $error; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<div class="container" style="margin-top: 40px;">
    <div class="row">
        <div class="panel panel-default panel-padding">
            <div class="panel-heading" style="margin-bottom: 20px;">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h1 class="panel-title"><i class="fa fa-bars"></i> <?php echo $page_title; ?></h1>
                        <p class="help-block">Click on the menu name to add links and items.</p>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success tooltips" href="<?php echo site_url('admin/menus/create');?>"><span class="glyphicon glyphicon-plus-sign"></span> Add new menu</a>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover-warning table-bordered table-hover list-info">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody class="sort-it">
                <?php if(!empty($menus)): ?>
                    <?php foreach($menus as $menu): ?>
                        <tr>
                            <td><?php echo $menu->id; ?></td>
                            <td><?php echo anchor('admin/menus/items/'.(int)$menu->id, $menu->title); ?></td>
                            <td>
                                <div class="text-center">
                                    <div class="btn-group">
                                        <?php echo anchor('admin/menus/edit/'.$menu->id, '<i class="fa fa-pencil-square-o"> Edit</i>', array('class' => 'btn btn-warning')); ?>
                                        <?php if($menu->id != 1) echo ' '.anchor('admin/menus/delete/'.$menu->id,'<i class="fa fa-trash"> Delete</i>', array('class' => 'btn btn-danger', 'onclick' => "return confirm('are you sure? This will delete all the items associated with this menu.')")); ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="3">
                            <div class="text-center">
                                <?php echo "No groups"; ?>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
