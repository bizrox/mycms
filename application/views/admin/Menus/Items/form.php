<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/3/2015
 * Time: 1:16 PM
 */
?>


<?php echo form_open('', array('role'=>'form', 'class' => 'form-horizontal')); ?>
<?php if (isset($menu_id) && isset($item_id) && isset($language_slug) ) : ?>
    <?php echo form_hidden('menu_id', $menu_id); ?>
    <?php echo form_hidden('item_id', $item_id); ?>
    <?php echo form_hidden('language_slug', $language_slug); ?>
<?php endif; ?>
<?php if (isset($translation[0]->id) ) : ?>
    <?php echo form_hidden('translation_id', $translation[0]->id); ?>
<?php endif; ?>
<div class="row">
    <h1><i class="fa fa-tasks"></i> <?php echo $page_title;  ?></h1>
    <hr/>
    <fieldset>
        <!-- Select Basic -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="default">Parent item</label>
            <div class="col-md-4">
                <?php

                $parent_id = isset($item[0]->parent_id) ? $item[0]->parent_id : '0';
                //var_dump($categories[''.$project['categories'].'']);
                //echo form_dropdown('default', array('0' => 'Not default', '1'=>'Default'),set_value('default',0),'class="form-control"');
                echo form_dropdown('parent_id', $parent_items, $parent_id, array('class' => 'form-control', 'required' => "")); ?>
                <!--<select id="category" name="category" class="form-control">
                </select>-->
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="title">title </label>
            <div class="col-md-4 <?php echo form_error('title') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'title',
                    'value'=>set_value('title', (isset($item[0]->title) ? $item[0]->title : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Title')); ?>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="url">URL</label>
            <div class="col-md-4 <?php echo form_error('url') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'url',
                    'value'=>set_value('url', (isset($item[0]->url) ? $item[0]->url : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Url')); ?>
            </div>
            <div class="checkbox col-md-4">
            <label><input type="checkbox" name="absolute_path" value="0" <?php echo (isset($item[0]->absolute_path) && $item[0]->absolute_path == 1 ) ? 'checked' : '' ?> <?php echo  set_radio('absolute_path', '0'); ?> />Absolute path</label>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="url">Css class<p class="help-block">This is for the icon above the menu text</p></label>

            <div class="col-md-4 <?php echo form_error('styling') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'css-class',
                    'value'=>set_value('css-class', (isset($item[0]->styling) ? $item[0]->styling : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'css class')); ?>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="order">Order </label>
            <div class="col-md-4 <?php echo form_error('order') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'order',
                    'value'=>set_value('order', (isset($item[0]->order) ? $item[0]->order : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Order')); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="">Select for which users you want to display the menu item <p class="help-block">leaving them both unchecked will be displayed for both categories. Checking them both will have the same effect</p></label>
            <div class="col-md-4 <?php //echo form_error('is_active') ? ' has-error' : ''; ?>">
                <?php //var_dump($item); ?>
                <div class="checkbox">
                    <label><input type="checkbox" name="logged-in-only" value="1" <?php echo (isset($item[0]->logged_in_only) && $item[0]->logged_in_only == 1 ) ? 'checked' : '' ?>  <?php echo  set_checkbox('logged-in-only', '1'); ?> />Available for logged in users only</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="logged-out-only" value="1"  <?php echo (isset($item[0]->logged_out_only) && $item[0]->logged_out_only == 1 ) ? 'checked' : '' ?>  <?php echo  set_checkbox('logged-out-only', '1'); ?> />Available for logged out users only</label>
                </div>
                <!--<div class="checkbox">
                    <label><input type="checkbox" name="both" value="1"  <?php /*echo (isset($project['is_active']) && $project['is_active'] == 1 ) ? 'checked' : '' */?>  <?php /*echo  set_checkbox('both', '1'); */?> />Available for both logged in and logged out users</label>
                </div>-->
            </div>
        </div>
    </fieldset>
    <!-- Button (Double) -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="submit_group"></label>
        <div class="col-md-8">
            <button id="submit_group" name="submit_group" class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
