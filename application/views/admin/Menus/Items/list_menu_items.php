<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/3/2015
 * Time: 11:43 AM
 */
?>


<?php if ($this->session->flashdata('errors')) : ?>
    <?php foreach ( $this->session->flashdata('errors') as $error ): ?>
        <div class="row alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $error; ?>
        </div>
    <?php endforeach; ?>
<?php endif;
//var_dump($langs);die();
?>
<div class="container" style="margin-top: 40px;">
    <div class="row">
        <div class="panel panel-default panel-padding">
            <div class="panel-heading" style="margin-bottom: 20px;">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h1 class="panel-title"><i class="fa fa-tasks"></i> <?php echo $page_title; ?></h1>
                    </div>
                    <div class="col-md-6 text-right">
                        <!-- Single button -->
                        <?php
                        if(count($langs) > 1) ://we have more than one language?>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false">Add item to <?php echo $menu->title;?>
                                        <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <?php
                                    foreach ($langs as $slug => $language) {
                                        echo '<li>' . anchor('admin/menus/createItem/' . $menu->id . '/' . $slug, $language['name']) . '</li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                        <?php else:
                            echo anchor('admin/menus/createItem/' . $menu->id . '/' .$current_lang['slug'],'Add item to '.$menu->title,'class="btn btn-primary"');
                        ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover-warning table-bordered table-hover list-info">
                <thead>
                <tr>
                    <th rowspan="2">Id</th>
                    <th rowspan="2">Item title</th>
                    <th rowspan="2">Item parent</th>
                    <?php foreach($langs as $slug => $language): ?>
                        <th><?php echo $slug; ?></th>
                    <?php endforeach; ?>
                    <th rowspan="2"></th>
                </tr>
                <tr>
                    <?php foreach($langs as $slug => $language): ?>
                        <th class="text-center">Actions</th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody class="sort-it">
                <?php if(!empty($items)): ?>
                    <?php foreach($items as $item_id => $item): ?>
                        <tr>
                            <td><?php echo $item_id; ?></td>
                            <td><?php echo $item['title']; ?></td>
                            <td><?php echo $item['parent_id']; ?></td>
                            <?php foreach($langs as $slug => $language): ?>
                                <td>
                                    <?php if (array_key_exists($slug, $item['translations'])): //let him edit or delete it ?>
                                       <?php echo anchor('admin/menus/editItem/'.$item['menu_id'].'/'.$slug.'/'.$item_id,' <i class="fa fa-pencil-square-o"> Edit</i>', array('class' => 'btn btn-warning')); ?>
                                       <?php echo anchor('admin/menus/deleteItem/'.$item['menu_id'].'/'.$slug.'/'.$item_id,' <i class="fa fa-trash"> Delete</i>', array('class' => 'btn btn-danger', 'onclick' => "return confirm('are you sure?')")); ?>
                                        <?php else: ?>
                                        <?php echo anchor('admin/menus/createItem/'.$item['menu_id'].'/'.$slug.'/'.$item_id,' <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Create', array('class' => 'btn btn-success')); ?>
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <td>
                                <?php echo anchor('admin/menus/deleteItem/'.$item['menu_id'].'/all/'.$item_id,'<i class="fa fa-trash"> Delete</i>', array('class' => 'btn btn-danger', 'onclick' => "return confirm('are you sure?')")); ?>
                            </td>
                           <!-- <td>
                                <div class="text-center">
                                    <div class="btn-group">
                                        <?php /*echo anchor('admin/menus/edit/'.$menu->id, '<span class="glyphicon glyphicon-pencil"></span>', array('class' => 'btn btn-warning')); */?>
                                        <?php /*if($menu->id != 1) echo ' '.anchor('admin/groups/delete/'.$menu->id,'<span class="glyphicon glyphicon-trash"></span>', array('class' => 'btn btn-danger', 'onclick' => "return confirm('are you sure?')")); */?>
                                    </div>
                                </div>
                            </td>-->
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="3">
                            <div class="text-center">
                                <?php echo "No Items"; ?>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>
