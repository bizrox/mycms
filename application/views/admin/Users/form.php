<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 10/30/2015
 * Time: 1:48 PM
 */
?>

<?php echo form_open('', array('role'=>'form', 'class' => 'form-horizontal')); ?>
<?php if (isset($user->id)) : ?>
    <?php echo form_hidden('user_id', $user->id); ?>
<?php endif; ?>

<div class="row">
    <h1><i class="fa fa-user-plus"></i> <?php echo $page_title; ?></h1>
    <hr/>
    <fieldset>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="first_name">First name </label>
            <div class="col-md-4 <?php echo form_error('first_name') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'first_name',
                    'value'=>set_value('first_name', (isset($user->first_name) ? $user->first_name : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'First name')); ?>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="last_name">Last name</label>
            <div class="col-md-4 <?php echo form_error('last_name') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'last_name',
                    'value'=>set_value('last_name', (isset($user->last_name) ? $user->last_name : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Last name')); ?>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-4 <?php echo form_error('email') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'email',
                    'value'=>set_value('email', (isset($user->email) ? $user->email : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'E-mail')); ?>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-4 <?php echo form_error('password') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_password(array(
                    'name'=>'password',
                    'class'=>'form-control input-md',
                    //'required' => '',
                    'placeholder' => 'Password')); ?>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="confirm_password">Confirm password</label>
            <div class="col-md-4 <?php echo form_error('confirm_password') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_password(array(
                    'name'=>'confirm_password',
                    'class'=>'form-control input-md',
                    //'required' => '',
                    'placeholder' => 'Confirm password')); ?>
            </div>
        </div>
        <!-- Select Basic -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="groups">Groups</label>
            <div class="col-md-4">
                <?php
                $groups = (isset($group_names) && !empty($group_names)) ? $group_names : array();
                $user_groups = (isset($user_groups) && !empty($user_groups)) ? $user_groups : "1";
                //var_dump($categories[''.$project['categories'].'']);
                echo form_dropdown('groups[]', $groups, $user_groups, array('class' => 'form-control', 'multiple' => 'multiple', 'required' => "")); ?>
                <!--<select id="category" name="category" class="form-control">
                </select>-->
            </div>
        </div>
    </fieldset>
    <!-- Button (Double) -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="submit_group"></label>
        <div class="col-md-8">
            <button id="submit_group" name="submit_group" class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
