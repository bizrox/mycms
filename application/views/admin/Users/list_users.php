<?php
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 10/30/2015
 * Time: 1:01 PM
 */
?>
<?php if ($this->session->flashdata('errors')) : ?>
    <?php foreach ( $this->session->flashdata('errors') as $error ): ?>
        <div class="row alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $error; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<div class="container" style="margin-top: 40px;">
    <div class="row">
        <div class="panel panel-default panel-padding">
            <div class="panel-heading" style="margin-bottom: 20px;">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h1 class="panel-title"><i class="fa fa-users"></i> <?php echo $page_title; ?></h1>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success tooltips" href="<?php echo site_url('admin/users/create');?>"><i class="fa fa-user-plus"></i> Add new user</a>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover-warning table-bordered table-hover list-info" >
                <thead>
                <tr>
                    <th>Id</th>
                    <!--<th>Username</th>-->
                    <th>Name</th>
                    <th>Email</th>
                    <th>Last login</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody class="sort-it">
                <?php if(!empty($users)): ?>
                    <?php foreach($users as $user): ?>
                        <tr>
                            <td><?php echo $user->id; ?></td>
                            <td><?php echo $user->first_name .' '. $user->last_name; ?></td>
                            <td><?php echo $user->email; ?></td>
                            <td><?php date('Y-m-d H:i:s', $user->last_login) ?></td>
                            <td>
                                <div class="text-center">
                                    <div class="btn-group">
                                        <?php echo anchor('admin/users/edit/'.$user->id, '<i class="fa fa-pencil-square-o"> Edit</i>', array('class' => 'btn btn-warning')); ?>
                                        <?php if( $current_user->id != $user->id ) echo ' '.anchor('admin/users/delete/'.$user->id,'<i class="fa fa-trash"> Delete</i>', array('class' => 'btn btn-danger', 'onclick' => "return confirm('are you sure?')")); ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="6">
                            <div class="text-center">
                                <?php echo "No users"; ?>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php if(isset($pagination)): ?>
                        <?php echo $pagination; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
