<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/2/2015
 * Time: 12:08 PM
 */
?>

<?php echo form_open('', array('role'=>'form', 'class' => 'form-horizontal')); ?>
<?php if (isset($language->id)) : ?>
    <?php echo form_hidden('language_id', $language->id); ?>
<?php endif; ?>

<div class="row">
    <h1><i class="fa fa-language"></i> <?php echo $page_title; ?></h1>
    <hr/>
    <fieldset>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="language_name">Language name</label>
            <div class="col-md-4 <?php echo form_error('language_name') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'language_name',
                    'value'=>set_value('language_name', (isset($language->language_name) ? $language->language_name : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Language Name')); ?>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="language_slug">Language slug <span class="help-block">(en, fr, ro etc.)</span></label>
            <div class="col-md-4 <?php echo form_error('language_slug') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'language_slug',
                    'value'=>set_value('description', (isset($language->slug) ? $language->slug : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Language slug')); ?>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="language_directory">Language directory</label>
            <div class="col-md-4 <?php echo form_error('language_directory') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'language_directory',
                    'value'=>set_value('description', (isset($language->language_directory) ? $language->language_directory : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Language directory')); ?>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="language_code">Language code<span class="help-block">(en, fr, ro etc.)</span></label>
            <div class="col-md-4 <?php echo form_error('language_code') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'language_code',
                    'value'=>set_value('description', (isset($language->language_code) ? $language->language_code : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Language code')); ?>
            </div>
        </div>
        <!-- Select Basic -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="default">Default language</label>
            <div class="col-md-4">
                <?php
                $defaults = array('0' => 'Not default', '1'=>'Default');
                $language_is_default = (isset($language_is_default) && !empty($language_is_default)) ? $language_is_default : "0";
                //var_dump($categories[''.$project['categories'].'']);
                //echo form_dropdown('default', array('0' => 'Not default', '1'=>'Default'),set_value('default',0),'class="form-control"');
                echo form_dropdown('default', $defaults, $language_is_default, array('class' => 'form-control', 'required' => "")); ?>
                <!--<select id="category" name="category" class="form-control">
                </select>-->
            </div>
        </div>
    </fieldset>
    <!-- Button (Double) -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="submit_group"></label>
        <div class="col-md-8">
            <button id="submit_group" name="submit_group" class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

