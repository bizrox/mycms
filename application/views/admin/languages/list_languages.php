<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/2/2015
 * Time: 11:58 AM
 */
?>

<?php if ($this->session->flashdata('errors')) : ?>
    <?php foreach ( $this->session->flashdata('errors') as $error ): ?>
        <div class="row alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $error; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<div class="container" style="margin-top: 40px;">
    <div class="row">
        <div class="panel panel-default panel-padding">
            <div class="panel-heading" style="margin-bottom: 20px;">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h1 class="panel-title"><i class="fa fa-language"></i> <?php echo $page_title; ?></h1>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success tooltips" href="<?php echo site_url('admin/languages/create');?>"><i class="fa fa-plus"></i> Add new language</a>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover-warning table-bordered table-hover list-info">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Code</th>
                    <th>Directoty</th>
                    <th>Is default</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody class="sort-it">
                <?php if(!empty($languages)): ?>
                    <?php foreach($languages as $language): ?>
                        <tr>
                            <td><?php echo $language->id; ?></td>
                            <td><?php echo $language->language_name; ?></td>
                            <td><?php echo $language->slug; ?></td>
                            <td><?php echo $language->language_code; ?></td>
                            <td><?php echo $language->language_directory; ?></td>
                            <td><?php echo $language->default; ?></td>
                            <td>
                                <div class="text-center">
                                    <div class="btn-group">
                                        <?php echo anchor('admin/languages/edit/'.(int)$language->id, '<i class="fa fa-pencil-square-o"> Edit</i>', array('class' => 'btn btn-warning')); ?>
                                        <?php echo ' '.anchor('admin/languages/delete/'.(int)$language->id,'<i class="fa fa-trash"> Delete</i>', array('class' => 'btn btn-danger', 'onclick' => "return confirm('are you sure?')")); ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="7">
                            <div class="text-center">
                                <?php echo "No languages"; ?>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>