<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/17/2015
 * Time: 11:08 AM
 */
?>


<?php echo form_open('', array('role'=>'form', 'class' => 'form-horizontal')); ?>
<?php if (isset($page_id) && isset($language_slug) ) : ?>
    <?php echo form_hidden('page_id', $page_id); ?>
    <?php echo form_hidden('language_slug', $language_slug); ?>
<?php endif; ?>
<?php if (isset($translation[0]->id) ) : ?>
    <?php echo form_hidden('translation_id', $translation[0]->id); ?>
<?php endif; ?>
<div class="row">
    <h1><i class="fa fa-book"></i> <?php echo $page_title;  ?></h1>
    <hr/>
    <div class="row">
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">title </label>
            <div class="col-md-4 <?php echo form_error('title') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'title',
                    'value'=>set_value('title', (isset($page[0]->title) ? $page[0]->title : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Title')); ?>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-2 control-label" for="short_title">Short Title</label>
            <div class="col-md-4 <?php echo form_error('short_title') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'short_title',
                    'value'=>set_value('short_title', (isset($page[0]->short_title) ? $page[0]->short_title : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Short title')); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="is_active">Is the page active? </label>
            <div class="col-md-4 <?php //echo form_error('is_active') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                <?php
                //$checked = (isset($project['is_active']) && !empty($project['is_active'])) ? $project['is_active'] : "";
                //print_r($project['is_active']);
                ?>
                <div class="radio">
                    <label><input type="radio" name="is_active" value="0" <?php echo (isset($page[0]->is_active) && $page[0]->is_active == 0 ) ? '' : 'checked' ?> required <?php echo  set_radio('is_active', '0'); ?> />No</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="is_active" value="1"  <?php echo (isset($page[0]->is_active) && $page[0]->is_active == 1 ) ? 'checked' : '' ?> required <?php echo  set_radio('is_active', '1'); ?> />Yes</label>
                </div>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-2 control-label" for="content">Content <p class="help-block">Must not contain more than one "Heading 1"</p></label>
            <div class="col-md-10 <?php echo form_error('content') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'content',
                    'id'  => 'page-content',
                    'value'=>set_value('content', (isset($page[0]->content) ? $page[0]->content : '')),
                    'class'=>'form-control input-md',
                    //'required' => '',
                    'placeholder' => 'Content')); ?>
            </div>
        </div>

        </div>
        <hr/>
        <div class="row">
            <h1><i class="fa fa-google"></i> SEO info</h1>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-2 control-label" for="page_description">Page description</label>
                <div class="col-md-10 <?php echo form_error('page_description') ? ' has-error' : ''; ?>">
                    <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                    <?php echo form_input(array(
                        'name'=>'page_description',
                        'id'  => 'page-description',
                        'value'=>set_value('page_description', (isset($page[0]->page_description) ? $page[0]->page_description : '')),
                        'class'=>'form-control input-md',
                        'required' => '',
                        'placeholder' => 'Page description')); ?>
                    <p class="help-block">Should be unique on every page and must not exceed 155 characters</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="page_keywords">Page keywords</label>
                <div class="col-md-10 <?php echo form_error('page_description') ? ' has-error' : ''; ?>">
                    <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                    <?php echo form_input(array(
                        'name'=>'page_keywords',
                        'id'  => 'page-keywords',
                        'value'=>set_value('page_keywords', (isset($page[0]->page_keywords) ? $page[0]->page_keywords : '')),
                        'class'=>'form-control input-md',
                        'required' => '',
                        'placeholder' => 'Page keywords')); ?>
                    <p class="help-block">Must not exceed 255 characters</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="page_title">Page title</label>
                <div class="col-md-10 <?php echo form_error('page_title') ? ' has-error' : ''; ?>">
                    <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->

                    <?php echo form_input(array(
                        'name'=>'page_title',
                        'id'  => 'page-title',
                        'value'=>set_value('page_title', (isset($page[0]->page_title) ? $page[0]->page_title : '')),
                        'class'=>'form-control input-md',
                        'required' => '',
                        'placeholder' => 'Page title')); ?>
                    <p class="help-block">Must contain keywords, keyword phrases(separated by "|") and must not exceed 64 characters. Example: cer albastru | pamant | glob</p>
                </div>
            </div>
        </div>
    <!-- Button (Double) -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="submit_group"></label>
        <div class="col-md-8">
            <button id="submit_group" name="submit_group" class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

