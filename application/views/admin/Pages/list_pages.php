<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 11/16/2015
 * Time: 2:18 PM
 */
?>


<?php if ($this->session->flashdata('errors')) : ?>
    <?php foreach ( $this->session->flashdata('errors') as $error ): ?>
        <div class="row alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $error; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<div class="container" style="margin-top: 40px;">
    <div class="row">
        <div class="panel panel-default panel-padding">
            <div class="panel-heading" style="margin-bottom: 20px;">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h1 class="panel-title"><i class="fa fa-bars"></i> <?php echo $page_title; ?></h1>
                    </div>
                    <div class="col-md-6 text-right">
                        <!-- Single button -->
                        <?php
                            if(count($langs) > 1) ://we have more than one language ?>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">Add Page
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <?php
                                        foreach ($langs as $slug => $language) {
                                            echo '<li>' . anchor('admin/pages/createPage/' . $slug, $language['name']) . '</li>';
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php else:
                                echo anchor('admin/menus/createItem/' .$current_lang['slug'],'Add page ','class="btn btn-primary"');
                                ?>
                            <?php endif; ?>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover-warning table-bordered table-hover list-info">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody class="sort-it">
                <?php if(!empty($pages)): ?>
                    <?php foreach($pages as $page): ?>
                        <tr>
                            <td><?php echo $page->id; ?></td>
                            <td><?php echo anchor('admin/pages/'.(int)$page->id); ?></td>
                            <td>
                                <div class="text-center">
                                    <div class="btn-group">
                                        <?php echo anchor('admin/menus/edit/'.$page->id, '<i class="fa fa-pencil-square-o"> Edit</i>', array('class' => 'btn btn-warning')); ?>
                                        <?php if($page->id != 1) echo ' '.anchor('admin/menus/delete/'.$page->id,'<i class="fa fa-trash"> Delete</i>', array('class' => 'btn btn-danger', 'onclick' => "return confirm('are you sure? This will delete all the items associated with this menu.')")); ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="3">
                            <div class="text-center">
                                <?php echo "No pages"; ?>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
