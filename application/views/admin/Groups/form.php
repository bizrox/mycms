<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 10/30/2015
 * Time: 1:48 PM
 */
?>

<?php echo form_open('', array('role'=>'form', 'class' => 'form-horizontal')); ?>
<?php if (isset($group->id)) : ?>
    <?php echo form_hidden('group_id', $group->id); ?>
<?php endif; ?>

<div class="row">
    <h1><i class="fa fa-users"></i> <?php echo $page_title; ?></h1>
    <hr/>
    <fieldset>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="group_name">Group name </label>
            <div class="col-md-4 <?php echo form_error('group_name') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_ro" name="project_name_ro" placeholder="Project name RO" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'group_name',
                    'value'=>set_value('group_name', (isset($group->name) ? $group->name : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Group Name')); ?>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="description">Group description</label>
            <div class="col-md-4 <?php echo form_error('description') ? ' has-error' : ''; ?>">
                <!--<input id="project_name_en" name="project_name_en" placeholder="Project name EN" class="form-control input-md" required type="text">-->
                <?php echo form_input(array(
                    'name'=>'description',
                    'value'=>set_value('description', (isset($group->description) ? $group->description : '')),
                    'class'=>'form-control input-md',
                    'required' => '',
                    'placeholder' => 'Group description')); ?>
            </div>
        </div>
    </fieldset>
    <!-- Button (Double) -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="submit_group"></label>
        <div class="col-md-8">
            <button id="submit_group" name="submit_group" class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
