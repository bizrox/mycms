<?php
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 10/30/2015
 * Time: 1:01 PM
 */
?>
<?php if ($this->session->flashdata('errors')) : ?>
    <?php foreach ( $this->session->flashdata('errors') as $error ): ?>
        <div class="row alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $error; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<div class="container" style="margin-top: 40px;">
    <div class="row">
        <div class="panel panel-default panel-padding">
            <div class="panel-heading" style="margin-bottom: 20px;">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h1 class="panel-title"><i class="fa fa-users"></i> <?php echo $page_title; ?></h1>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success tooltips" href="<?php echo site_url('admin/groups/create');?>"><i class="fa fa-plus-circle"></i> Add new group</a>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover-warning table-bordered table-hover list-info">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody class="sort-it">
                    <?php if(!empty($groups)): ?>
                        <?php foreach($groups as $group): ?>
                        <tr>
                            <td><?php echo $group->id; ?></td>
                            <td><?php echo $group->name; ?></td>
                            <td><?php echo $group->description; ?></td>
                            <td>
                                <div class="text-center">
                                    <div class="btn-group">
                                        <?php echo anchor('admin/groups/edit/'.$group->id, '<i class="fa fa-pencil-square-o"> Edit</i>', array('class' => 'btn btn-warning')); ?>
                                        <?php if(!in_array($group->name, array('admin', 'members'))) echo ' '.anchor('admin/groups/delete/'.$group->id,'<i class="fa fa-trash"> Delete</i>', array('class' => 'btn btn-danger', 'onclick' => "return confirm('are you sure?')")); ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="4">
                                <div class="text-center">
                                    <?php echo "No groups"; ?>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
