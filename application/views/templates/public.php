<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 29.01.2016
 * Time: 14:07
 */


$this->load->view('templates/partials/public_header'); ?>

	<?php if( strtolower($page_title) == 'homepage' ): ?>
        <div class="visible-phone" style="margin-top: 50px;"></div>
		 <div class="homepage" style="position: relative; bottom: 30px; width: 100%;">
            <?php echo $the_view_content; ?>
		 </div>
	<?php else: ?>
    <div class="container">
        <div class="main-content" style="padding-top:40px;">
            <?php echo $the_view_content; ?>
        </div>
    </div>
   <?php endif; ?>
<?php $this->load->view('templates/partials/public_footer');?>