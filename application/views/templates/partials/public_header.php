<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 29.01.2016
 * Time: 14:08
 */
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="">
    <link rel="icon" type="image/x-icon" href="">
    <title><?php echo $page_title; ?> - <?php echo $this->settings->site_name; ?></title>
    <meta name="keywords" content="<?php //echo $this->settings->meta_keywords; ?>">
    <meta name="description" content="<?php //echo $this->settings->meta_description; ?>">
    <?php if (isset($css_files) && is_array($css_files)) : ?>
        <?php foreach ($css_files as $css) : ?>
            <?php if ( ! is_null($css)) : ?>
                <link rel="stylesheet" href="<?php echo $css; ?>"><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (isset($js_files) && is_array($js_files)) : ?>
            <?php foreach ($js_files as $js) : ?>
                <?php if ( ! is_null($js)) : ?>
                    <?php echo "\n"; ?><script type="text/javascript" src="<?php echo $js; ?>"></script><?php echo "\n"; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body><?php //print_r($current_lang); ?>
<div class="container header-container">
            <div class="row">
                <div class="span16">
                    <!-- Header -->
                    <div id="dn-header">
                        <div class="dn-free-consult">
                            <?php if ( $current_lang['slug'] == 'ro'): ?>
                                <?php  if ( $this->session->has_userdata('user_info') ): ?>
                                    <a class="link" href="<?php echo $current_lang['slug'] ?>/autentificare/cont-nou">Profil</a> |
                                    <a class="link" href="<?php echo $current_lang['slug'] ?>/autentificare/logout">Logout</a>
                                <?php else: ?>
                                    <a class="link" href="<?php echo $current_lang['slug'] ?>/autentificare/cont-nou">Cont nou</a> |
                                    <a class="link" href="<?php echo $current_lang['slug'] ?>/autentificare/login">Login</a>
                                <?php endif; ?>
                                <?php else: ?>
                                    <?php  if ( $this->session->has_userdata('user_info') ): ?>
                                        <a class="link" href="<?php echo $current_lang['slug'] ?>/autentificare/cont-nou">Profile</a> |
                                        <a class="link" href="<?php echo $current_lang['slug'] ?>/autentificare/login">Logou</a>
                                        <?php else: ?>
                                        <a class="link" href="<?php echo $current_lang['slug'] ?>/autentificare/cont-nou">Cont nou</a> |
                                        <a class="link" href="<?php echo $current_lang['slug'] ?>/autentificare/login">Login</a>
                                    <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- Main Menu -->
                    <div id="dn-main-menu-background">
                        <div class="dn-logo span4 visible-desktop">
                            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/front/img/logo3.png" alt="TMM - logo" width="112" height="98"></a>
                        </div>
                        <div id="dn-main-menu" class="span11 dn-main-menu">
                            <ul class="menu-items">
                               <?php echo $top_menu; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
 <!-- <header id="header">
   <div class="container">
        <div class="row">
            <a class="navbar-brand" href="/">TMM</a>
            <div class="col-sm-12 mainmenu_wrap"><div class="main-menu-icon visible-xs "><span></span><span></span><span></span></div>
                <ul id="mainmenu" class="menu sf-menu responsive-menu superfish">
                    <?php echo $top_menu; ?>
                </ul>
            </div>
        </div>
    </div> 
</header>-->

<div class="container theme-showcase" role="main">
    <?php // System messages ?>
    <?php if ($this->session->flashdata('message')) : ?>
        <div class="row alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('message'); ?>
        </div>
    <?php elseif ($this->session->flashdata('error')) : ?>
        <div class="row alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php elseif (validation_errors()) : ?>
        <div class="row alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo validation_errors(); ?>
        </div>
    <?php elseif (isset($this->error)) : ?>
        <div class="row alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->error; ?>
        </div>
    <?php endif; ?>

</div>
