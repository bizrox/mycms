<?php
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 10/30/2015
 * Time: 12:52 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');?>
<li class="divider"></li>
<li><a href="<?php echo site_url('admin/groups'); ?>"><i class="fa fa-users"></i> Groups</a></li>
<li><a href="<?php echo site_url('admin/users/list_users'); ?>"><i class="fa fa-user"></i> Users</a></li>