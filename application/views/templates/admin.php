<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: paul.schiller
 * Date: 10/30/2015
 * Time: 11:30 AM
 */

$this->load->view('templates/partials/admin_header'); ?>

    <div class="container">
        <div class="main-content" style="padding-top:60px;">
            <?php echo $the_view_content; ?>
        </div>
    </div>
<?php $this->load->view('templates/partials/admin_footer');?>