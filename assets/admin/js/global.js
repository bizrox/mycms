/**
 * Created by paul.schiller on 11/13/2015.
 *
 */

var Application = {

    /**
     * in case we will use csrf token this will help with the ajax requests
     */
    /*appendLoadingjax: function() {
        jQuery.ajaxSetup({
            beforeSend: function(jqXHR, settings) {
                $('#loader').fadeIn('15000');
                //add csrf token to each call
                settings.data = settings.data +'&csrf_token='+$.cookie('csrf_cookie');
                return true;
            },
            complete: function(){
                $('#loader').fadeOut('slow');
            },
            success: function() {}
        });
    },*/

    //datatable
    initializeDataTable: function() {
        $('.list-info').DataTable();
    },
    //tinymce
    initializeTinyMce: function() {
        tinymce.init({
            selector: "#page-content",
            theme: "modern",
            skin: 'light',
            height: 500,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            image_advtab: true ,

            external_filemanager_path:"/filemanager/",
            filemanager_title:"Responsive Filemanager" ,
            external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
        });
    }

}


$(document).ready(function () {

    Application.initializeDataTable();
    Application.initializeTinyMce();
    /*$('body').append('</div><div id="loader"><img src="//www.ajaxload.info/cache/FF/FF/FF/00/00/00/1-0.gif"/></div>');
    Application.validateMacros();
    Application.changeLanguage();
    Application.registerForm();
    Application.appendLoadingjax();*/

});


