/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    /* Detect hi-res devices */
    function isHighDensity() {
        return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches)) || (window.devicePixelRatio && window.devicePixelRatio > 1.3));
    }

    var $body = $('body'),
        $baseUrl = 'http://localhost/tmm/';


var Journal = {

    init: function() {
        // get pie chart data
        self = this; 
        //self.content_preloader_show('md-preloader-danger', $('.md-card'));
        // $.ajax({
        //     url: 'jurnal/testing',
        //     type: 'GET',
        //     data: '', //form.serialize() ,
        //     success: function(result) {
        //         var $data = $.parseJSON(result);
        //         var pieData  = [
        //        {
        //           value: $data.data.proteins,
        //           label: 'Proteins',
        //           color: '#811BD6'
        //        },
        //        {
        //           value: $data.data.carbohydrates,
        //           label: 'Carbs',
        //           color: '#9CBABA'
        //        },
        //        {
        //           value: $data.data.fats,
        //           label: 'Fats',
        //           color: '#D18177'
        //        }
        //     ]
        //     var context = document.getElementById('skills').getContext('2d');
        //     console.log("sdf");
        //     context.canvas.width = 300;
        //     context.canvas.height = 300;
        //     var skillsChart = new Chart(context).Pie(pieData);
            
        //     self.content_preloader_hide();
        //         /*$form.data('formValidation').resetForm();
        //         var $data = $.parseJSON(result);
        //         if ( $data.result === true )
        //         {   
        //             //upodate the pie with the new values
        //             Application.updateMacrosPie($data.data);
        //             //update the inputs
        //             Application.updateInputs($data.data);
        //             //updating only the crrent day of the last week status
        //             //there is no need to update the whole chart
        //             //Application.updateLastWeekStatus($data.data);
        //             //success
        //             alertify.notify($data.message, 'custom', 4, function(){});
        //         }
        //         else
        //         {
        //             alertify.error($data.message, 4);//('Ready!');//console.log("success!");
        //         }*/
        //     }
        // });
    },
    /*
    *  style= md-preloader-warning, md-preloader-success, md-preloader-danger, md-preloader
     */
    content_preloader_show: function(style, container) {

        var image_density = isHighDensity() ? '@2x' : '' ;

        var preloader_content = '<div class="md-preloader '+style+'"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="32" width="32" viewbox="0 0 75 75"><circle cx="37.5" cy="37.5" r="33.5" stroke-width="8"/></svg></div>';

        var thisContainer = (typeof container !== 'undefined') ? container : $body;
        //console.log(preloader_content);
        thisContainer.append('<div class="content-preloader">' + preloader_content + '</div>');
        setTimeout(function() {
            $('.content-preloader').addClass('preloader-active');
        }, 0);
            
    },

    content_preloader_hide: function() {
        if($body.find('.content-preloader').length) {
            // hide preloader
            $('.content-preloader').removeClass('preloader-active');
            // remove preloader
            preloader_timeout = window.setTimeout(function() {
                $('.content-preloader').remove();
            }, 500);
        }
    },

    validateMacros: function (element, parent) {
        $('#macros').parsley();
        $("#macros").on('submit', function(e){
            e.preventDefault();
            var form = $(this);

            form.parsley().validate();

            if (form.parsley().isValid()){
                // Use Ajax to submit form data
                $.ajax({
                    url: 'jurnal/saveMacros',
                    type: 'POST',
                    data: $('input[name!=csrf_token]', form).serialize(), //form.serialize() ,
                    success: function(result) {
                        //console.log(result);
                        // $form.data('formValidation').resetForm();
                         var $data = $.parseJSON(result);
                        if ( $data.result === true )
                        {   
                            form.parsley().reset();
                            //$form.data('formValidation').resetForm();
                            //upodate the pie with the new values
                            Journal.updateMacrosPie($data.data);
                            //update the inputs
                            Journal.updateInputs($data.data);
                            //updating only the crrent day of the last week status
                            //there is no need to update the whole chart
                            //Application.updateLastWeekStatus($data.data);
                            //success
                            
                            alertify.success($data.message, 4, function(){});
                        }
                        else
                        {
                            alertify.error($data.message, 4);//('Ready!');//console.log("success!");
                        }
                    }
                });
            }
        });
        // $('#macros').formValidation({
        //     framework: 'bootstrap',
        //     icon: {
        //         valid: 'glyphicon glyphicon-ok',
        //         invalid: 'glyphicon glyphicon-remove',
        //         validating: 'glyphicon glyphicon-refresh'
        //     },
           
        //     fields: {
        //         proteins: {
        //             validators: {
        //                 notEmpty: {
        //                     message: 'This field cannot be empty'
        //                 },
        //                 regexp: {
        //                     regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
        //                     message: 'only positive numbers'
        //                 }
        //             }
        //         },
        //         carbohydrates: {
        //             validators: {
        //                 notEmpty: {
        //                     message: 'This field cannot be empty'
        //                 },
        //                 regexp: {
        //                     regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
        //                     message: 'only positive numbers'
        //                 }
        //             }
        //         },
        //         fats: {
        //             validators: {
        //                 notEmpty: {
        //                     message: 'This field cannot be empty'
        //                 },
        //                 regexp: {
        //                     regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
        //                     message: 'only positive numbers'
        //                 }
        //             }
        //         },
        //         calories: {
        //             validators: {
        //                 notEmpty: {
        //                     message: 'This field cannot be empty'
        //                 },
        //                 regexp: {
        //                     regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
        //                     message: 'only positive numbers'
        //                 }
        //             }
        //         },
        //     }
        // }).on('success.form.fv', function(e) {
        //     // Prevent form submission
        //     e.preventDefault();

        //     var $form = $(e.target),
        //         fv    = $form.data('formValidation');
            
        //     // Use Ajax to submit form data
        //     $.ajax({
        //         url: 'dashboard/saveMacros',
        //         type: 'POST',
        //         data: $('input[name!=csrf_token]', $form).serialize(), //form.serialize() ,
        //         success: function(result) {
        //             $form.data('formValidation').resetForm();
        //             var $data = $.parseJSON(result);
        //             if ( $data.result === true )
        //             {   
        //                 //upodate the pie with the new values
        //                 Application.updateMacrosPie($data.data);
        //                 //update the inputs
        //                 Application.updateInputs($data.data);
        //                 //updating only the crrent day of the last week status
        //                 //there is no need to update the whole chart
        //                 //Application.updateLastWeekStatus($data.data);
        //                 //success
        //                 alertify.notify($data.message, 'custom', 4, function(){});
        //             }
        //             else
        //             {
        //                 alertify.error($data.message, 4);//('Ready!');//console.log("success!");
        //             }
        //         }
        //     });
            
        // });
    },
    
    updateMacrosPie: function($macros) {
       
        skillsChart.segments[0].value = $macros.proteins;
        skillsChart.segments[1].value = $macros.carbohydrates;
        skillsChart.segments[2].value = $macros.fats;
        skillsChart.update();
        
    },
    
    updateInputs: function($macros) {
        //console.log($macros.proteins);
        $('#proteins').attr('value', $macros.proteins);
        $('#carbohydrates').attr('value', $macros.carbohydrates);
        $('#fats').attr('value',$macros.fats);
        $('#calories').attr('value',$macros.calories);
    },
    
    updateLastWeekStatus: function($macros) {
        
            /*var result = [];
            for (var i=0; i<7; i++) {
                var d = new Date();
                d.setDate(d.getDate() - i);
                result.push( Application.formatDate(d) )
            }
            result.join(',');*/
            //console.log(myLineChart.datasets[1].points[0].value );
            myLineChart.datasets[0].points[0].value = $macros.proteins;
            myLineChart.datasets[1].points[0].value = $macros.carbohydrates;
            myLineChart.datasets[2].points[0].value = $macros.fats;
            myLineChart.datasets[3].points[0].value = $macros.calories;
            myLineChart.update();
            
    },
    
    formatDate: function(date){
    
        var dd = date.getDate();
        var mm = date.getMonth()+1;
        var yyyy = date.getFullYear();
        if(dd<10) {dd='0'+dd}
        if(mm<10) {mm='0'+mm}
        date = ''+dd+'-'+mm+'-'+yyyy+'';
        return date
     },
    
    validateCustomMacros: function (element, parent) {
        $('#calc-macros').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
           
            fields: {
                custom_proteins: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                            message: 'only positive numbers'
                        }
                    }
                },
                custom_carbohydrates: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                            message: 'only positive numbers'
                        }
                    }
                },
                custom_fats: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                            message: 'only positive numbers'
                        }
                    }
                },
                custom_calories: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                            message: 'only positive numbers'
                        }
                    }
                },
                custom_grams: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                            message: 'only positive numbers'
                        }
                    }
                },
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var $form = $(e.target),
                fv    = $form.data('formValidation');
            
            // Use Ajax to submit form data
            $.ajax({
                url: 'dashboard/calculateMacros',
                type: 'POST',
                data: $('input[name!=csrf_token]', $form).serialize(), //form.serialize() ,
                success: function(result) {//alert("worked!");
                    $form.data('formValidation').resetForm();
                    var $data = $.parseJSON(result);
                    if ( $data.result === true )
                    {   
                        Application.displayCustomMacros($data.data);
                        alertify.notify($data.message, 'custom', 4, function(){});
                    }
                    else
                    {
                        alertify.error($data.message, 4);//('Ready!');//console.log("success!");
                    }
                }
            });
            
        });
    },
    
    displayCustomMacros: function($customMacros){   
        console.log($customMacros);
        $('form#calc-macros').before('<div class="col-xs-12">Macros are: <p class="alert alert-success">Proteins: '+$customMacros.custom_proteins+'<br/>Carbs: '+$customMacros.custom_carbohydrates+'<br/>fats: '+$customMacros.custom_fats+'<br/>calories: '+$customMacros.custom_calories+'</p></div>');
        //alert($data);
    },

}


$(document).ready(function () {
    
//    $('body').append('</div><div id="loader"><img src="//www.ajaxload.info/cache/FF/FF/FF/00/00/00/1-0.gif"/></div>');
    Journal.validateMacros();
    //Application.changeLanguage();
    //Application.registerForm();
    //Application.appendLoadingjax();
    //Application.validateCustomMacros();
    //Application.activeListItem();
    //Application.centerMobileMenu();
    //Application.initBxSlider();
    //Application.activeListItem();
    Journal.init();
});


