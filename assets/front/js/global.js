/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Application = {
    
    
    appendLoadingjax: function() {
      jQuery.ajaxSetup({
          beforeSend: function(jqXHR, settings) {
             $('#loader').fadeIn('15000');
             //add csrf token to each call
             settings.data = settings.data +'&csrf_token='+$.cookie('csrf_cookie');
             return true;
          },
          complete: function(){
             $('#loader').fadeOut('slow');
          },
          success: function() {}
        });
    },
    
    validateMacros: function (element, parent) {
        // $('#macros').formValidation({
        //     framework: 'bootstrap',
        //     icon: {
        //         valid: 'glyphicon glyphicon-ok',
        //         invalid: 'glyphicon glyphicon-remove',
        //         validating: 'glyphicon glyphicon-refresh'
        //     },
           
        //     fields: {
        //         proteins: {
        //             validators: {
        //                 notEmpty: {
        //                     message: 'This field cannot be empty'
        //                 },
        //                 regexp: {
        //                     regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
        //                     message: 'only positive numbers'
        //                 }
        //             }
        //         },
        //         carbohydrates: {
        //             validators: {
        //                 notEmpty: {
        //                     message: 'This field cannot be empty'
        //                 },
        //                 regexp: {
        //                     regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
        //                     message: 'only positive numbers'
        //                 }
        //             }
        //         },
        //         fats: {
        //             validators: {
        //                 notEmpty: {
        //                     message: 'This field cannot be empty'
        //                 },
        //                 regexp: {
        //                     regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
        //                     message: 'only positive numbers'
        //                 }
        //             }
        //         },
        //         calories: {
        //             validators: {
        //                 notEmpty: {
        //                     message: 'This field cannot be empty'
        //                 },
        //                 regexp: {
        //                     regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
        //                     message: 'only positive numbers'
        //                 }
        //             }
        //         },
        //     }
        // }).on('success.form.fv', function(e) {
        //     // Prevent form submission
        //     e.preventDefault();

        //     var $form = $(e.target),
        //         fv    = $form.data('formValidation');
            
        //     // Use Ajax to submit form data
        //     $.ajax({
        //         url: 'dashboard/saveMacros',
        //         type: 'POST',
        //         data: $('input[name!=csrf_token]', $form).serialize(), //form.serialize() ,
        //         success: function(result) {
        //             $form.data('formValidation').resetForm();
        //             var $data = $.parseJSON(result);
        //             if ( $data.result === true )
        //             {   
        //                 //upodate the pie with the new values
        //                 Application.updateMacrosPie($data.data);
        //                 //update the inputs
        //                 Application.updateInputs($data.data);
        //                 //updating only the crrent day of the last week status
        //                 //there is no need to update the whole chart
        //                 //Application.updateLastWeekStatus($data.data);
        //                 //success
        //                 alertify.notify($data.message, 'custom', 4, function(){});
        //             }
        //             else
        //             {
        //                 alertify.error($data.message, 4);//('Ready!');//console.log("success!");
        //             }
        //         }
        //     });
            
        // });
    },
    
    changeLanguage: function() {
        $('.language ul li').click(function(){
            var queryString = '';
            /*var data = new Array();
            data.push();
            console.log(data.serializeArray());*/
            queryString += 'lang=' + $(this).children('span').attr('lang');
            //console.log(queryString);
            $.ajax({
                url: 'dashboard/changeLanguage',
                type: 'POST',
                data: queryString, //form ,
                success: function(result) {
                    //$form.data('formValidation').resetForm();
                    var $data = $.parseJSON(result);
                    if ( $data.result === true )
                    {   
                        
                        location.reload();
                        //alertify.notify($data.message, 'custom', 4, function(){});
                    }
                    else
                    {
                        //alertify.error($data.message, 4);//('Ready!');//console.log("success!");
                    }
                }
            });
        });
    },
    
    updateMacrosPie: function($macros) {
       
        skillsChart.segments[0].value = $macros.proteins;
        skillsChart.segments[1].value = $macros.carbohydrates;
        skillsChart.segments[2].value = $macros.fats;
        skillsChart.update();
        
    },
    
    updateInputs: function($macros) {
        //console.log($macros.proteins);
        $('#proteins').attr('value', $macros.proteins);
        $('#carbohydrates').attr('value', $macros.carbohydrates);
        $('#fats').attr('value',$macros.fats);
        $('#calories').attr('value',$macros.calories);
    },
    
    updateLastWeekStatus: function($macros) {
        
            /*var result = [];
            for (var i=0; i<7; i++) {
                var d = new Date();
                d.setDate(d.getDate() - i);
                result.push( Application.formatDate(d) )
            }
            result.join(',');*/
            //console.log(myLineChart.datasets[1].points[0].value );
            myLineChart.datasets[0].points[0].value = $macros.proteins;
            myLineChart.datasets[1].points[0].value = $macros.carbohydrates;
            myLineChart.datasets[2].points[0].value = $macros.fats;
            myLineChart.datasets[3].points[0].value = $macros.calories;
            myLineChart.update();
            
    },
    
    formatDate: function(date){
    
        var dd = date.getDate();
        var mm = date.getMonth()+1;
        var yyyy = date.getFullYear();
        if(dd<10) {dd='0'+dd}
        if(mm<10) {mm='0'+mm}
        date = ''+dd+'-'+mm+'-'+yyyy+'';
        return date
     },
     
    registerForm: function() {
        
        $('.registration-form fieldset:first-child').fadeIn('slow');
        
        // next step
        $('.registration-form .btn-next').on('click', function(e) {
            	var parent_fieldset = $(this).parents('fieldset');
            	//console.log(parent_fieldset);
            	var next_step = true;
            	
        	    if( next_step ) {
            		parent_fieldset.fadeOut(400, function() {
            		    var $element = $(this).next();
        	    		$(this).next().fadeIn();
        	    		//console.log("next: "+$(this).index());
        	    		$('.'+$element.index()).addClass("active");
        	    		var $previous = $element.index()-1;
        	    		$('.'+$(this).index()).removeClass("active");
        	    		$('.'+$(this).index()).addClass("complete");
        	    		if ( !$element.find('*').hasClass('has-error') )
        	    		{  
        	    		    $('.registration-form input[type="submit"]').prop("disabled", false)
                            $('.registration-form input[type="submit"]').removeClass('disabled');
        	    		}
        	    		$('.registration-form').formValidation('validate');
        	    	});
        	    	
        	    }
    	    	if ( $(this).next().find('input.final-step') )
	    		{  console.log("foiund!");
	    		    $('.final-step').prop("disabled", false)
                    $('.final-step').removeClass('disabled');
	    		}
        });
        
        
        
        // previous step
        $('.registration-form .btn-previous').on('click', function(e) {
            $('.registration-form input[type="submit"]').each(function() {
                if ( $(this).prop('disabled', true) || $(this).hasClass('disabled') )
                {
                    $(this).prop("disabled", false)
                    $(this).removeClass('disabled');
                }
                //display all the inputs corresponding to the selected measuring unit
                
            });
        	$(this).parents('fieldset').fadeOut(400, function() {
        	    var $element2 = $(this).prev();
        	    //console.log("previous: "+$element2.index());
        	    
        		$(this).prev().fadeIn();
        		$('.'+$element2.index()).removeClass("complete");
        		$('.'+$element2.index()).addClass("active");
	    		var $previous = $(this).index();
	    		//console.log("current: "+$(this).index());
	    		$('.'+$previous).removeClass("active");
	    		
        	});
        });
        
        //Application.registerForm();
    
    $('.tooltip').tooltipster({
        position: 'right',
        contentAsHTML: true
    });
    //hide the div before the user selects a mesearing unit
    $('.wrapper').hide();
    
    $('.unit').click(function(){
        var $unitSystem = $(this).children('input').first().val();
        $('.registration-form').data('formValidation').resetForm();
        $(".wrapper input").each(function() {
            //display all the inputs corresponding to the selected measuring unit
            $('.wrapper .'+$unitSystem+'').show();
        });
        
        var $genderField = $('input[name="gender[]"]');
        var $weight = $('input[name="weight"]');
        /*$('.registration-form').formValidation('removeField', 'gender');
        $('.registration-form').formValidation('removeField', 'weight');*/
        $('.registration-form').formValidation('addField', 'gender');
        $('.registration-form').formValidation('addField', 'gender');
        $('.registration-form').formValidation('addField', $weight);
        $('.registration-form').formValidation('addField', $weight);
        
        
        //update the validator with the correct format for the date
        if ( $unitSystem == 'imperial' )
        {
            $('.registration-form .dob-tooltip').tooltipster('content', 'The date of birth should be in the format: '+$('input.'+$unitSystem+'-birthday').attr("placeholder")+'. <br/> Example 12/24/1985');
            $('.metric').hide();
            $('.registration-form .height-tooltip').tooltipster('content', 'Example: 6 2 (6ft2in)');
            //add validators dynamically
            var $imperialBirthdayField = $('input[name="birthday_imperial"]');
            var $heightFtField = $('input[name="height_ft"]');
            var $heightInField = $('input[name="height_in"]');
            $('.registration-form').formValidation('addField', $imperialBirthdayField);
            $('.registration-form').formValidation('addField', $imperialBirthdayField);
            $('.registration-form').formValidation('addField', $heightFtField);
            $('.registration-form').formValidation('addField', $heightFtField);
            $('.registration-form').formValidation('addField', $heightInField);
            $('.registration-form').formValidation('addField', $heightInField);
            //$('.registration-form').formValidation('removeField', 'height_cm');
            $('.registration-form').formValidation('removeField', 'birthday_metric');
            $('.registration-form').formValidation('removeField', 'height_cm');
            //$('.registration-form').formValidation('updateOption', 'birthday_'+$unitSystem+'', 'date', 'format', ''+$('input.'+$unitSystem+'-birthday').attr("placeholder")+'').formValidation('revalidateField', 'birthday_'+$unitSystem+'');
        }
        else
        {   
            $('.registration-form .dob-tooltip').tooltipster('content', 'The date of birth should be in the format: '+$('input.'+$unitSystem+'-birthday').attr("placeholder")+'. <br/> Example 24/12/1985');
            $('.imperial').hide();
            $('.registration-form .height-tooltip').tooltipster('content', 'The height should be in centimeters. Example: 180');
            //there might be a bug and I am adding the new field twice. Adding it only once and then validating it, won't work. 
            //There is a method down below which executes everytime you add a new field and I am inserting the new validators there
            var $metricBirthdayField = $('input[name="birthday_metric"]');
            var $heightCm = $('input[name="height_cm"]');
            $('.registration-form').formValidation('addField', $metricBirthdayField);
            $('.registration-form').formValidation('addField', $metricBirthdayField);
            $('.registration-form').formValidation('addField', $heightCm);
            $('.registration-form').formValidation('addField', $heightCm);
            $('.registration-form').formValidation('removeField', 'height_ft');
            $('.registration-form').formValidation('removeField', 'height_in');
            $('.registration-form').formValidation('removeField', 'birthday_imperial');
            
        }
       
        $('.wrapper').fadeIn('slow');
        $('.registration-form').formValidation('validate');
    });
    
    $('#register').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
           
            fields: {
                first_name: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[a-z\s]+$/i,
                            message: 'The first name can consist of alphabetical characters and spaces only'
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[a-z\s]+$/i,
                            message: 'The last name can consist of alphabetical characters and spaces only'
                        }
                    }
                },
                username: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9]+$/,
                            message: 'The username can consist of alphabetical characters and numerics only'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        stringLength: {
                            min: 5,
                            message: 'The password must be at least 5 character long'
                        }
                    }
                },
                password_repeat: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        identical: {
                            field: 'password',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                },
                unit: {
                    validators: {
                        notEmpty: {
                            message: ''
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            $('.final-step').click(function(){
                $('form#register').unbind('submit').submit();
                $('form#register').submit();
                return true;
                //$(this).unbind('submit').submit()
                // Use Ajax to submit form data
                /*$.ajax({
                    url: 'register',
                    type: 'POST',
                    data: $('input[name!=csrf_token]', $form).serialize(), //form.serialize() ,
                    success: function(result) {
                        // $form.data('formValidation').resetForm();
                        // var $data = $.parseJSON(result);
                    }
                });*/
            });
            e.preventDefault();
            $('.final-step').prop("disabled", false)
                    $('.final-step').removeClass('disabled');
        }).on('added.field.fv', function(e, data) {
            // $(e.target)  --> The form instance
            // $(e.target).data('formValidation')
            //              --> The FormValidation instance

            // data.field   --> The field name
            // data.element --> The new field element
            // data.options --> The new field options
            var  $dateValidators = {
                   validators:{
                    notEmpty: { 'message': "enter value bitch" }
                   }
                };
            var  $genderValidators = {
                   validators:{
                    notEmpty: { 'message': "Please select a value" }
                   }
                };
            var  $weightVlidators = {
                   validators:{
                    notEmpty: { 'message': "Please enter a value" },
                    regexp: {
                        regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                        message: 'only positive numbers'
                    }
                   }
                };
            var  $heightValidators = {
                   validators:{
                    notEmpty: { 'message': "enter value bitch" },
                    regexp: {
                        regexp: /^[1-9]\d*$/,//(this is for int numbers only)
                        message: 'only positive numbers'
                    }
                   }
                };    
                //console.log(data.field);
            switch (data.field) {
                
                case 'birthday_imperial': $dateValidators.validators.date = { 'format':"MM/DD/YYYY",  'message': "not a valid date" }; $.extend(data.options, $dateValidators); break;
                case 'birthday_metric': $dateValidators.validators.date = { 'format':"DD/MM/YYYY",  'message': "not a valid date" }; $.extend(data.options, $dateValidators); break;
                case 'height_cm': $.extend(data.options, $heightValidators); break;
                case 'height_ft': $.extend(data.options, $heightValidators); break;
                case 'height_in': $.extend(data.options, $heightValidators); break;
                case 'gender': $.extend(data.options, $genderValidators); break;
                case 'weight': $.extend(data.options, $weightVlidators); break;
                
            }
            //console.log($dateValidators);
            
            // $.extend(data.options, $genderValidators);
            // $.extend(data.options, $weightVlidators);
            // Do something ...
           
            
        }).on('err.form.fv', function(e) {//console.log("here error");
            //$('.registration-form .btn-next').unbind('click');
            //Application.registerForm(false);
            // The e parameter is same as one
            // in the prevalidate.form.fv event above

            // Do something ...
        });
        console.log("error?");
        $('.registration-form').formValidation('validate');
        
    },
    
    validateCustomMacros: function (element, parent) {
        $('#calc-macros').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
           
            fields: {
                custom_proteins: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                            message: 'only positive numbers'
                        }
                    }
                },
                custom_carbohydrates: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                            message: 'only positive numbers'
                        }
                    }
                },
                custom_fats: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                            message: 'only positive numbers'
                        }
                    }
                },
                custom_calories: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                            message: 'only positive numbers'
                        }
                    }
                },
                custom_grams: {
                    validators: {
                        notEmpty: {
                            message: 'This field cannot be empty'
                        },
                        regexp: {
                            regexp: /^[0-9]+([.][0-9]+)?$/,///^[1-9]\d*$/,(this is for int numbers only)
                            message: 'only positive numbers'
                        }
                    }
                },
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var $form = $(e.target),
                fv    = $form.data('formValidation');
            
            // Use Ajax to submit form data
            $.ajax({
                url: 'dashboard/calculateMacros',
                type: 'POST',
                data: $('input[name!=csrf_token]', $form).serialize(), //form.serialize() ,
                success: function(result) {//alert("worked!");
                    $form.data('formValidation').resetForm();
                    var $data = $.parseJSON(result);
                    if ( $data.result === true )
                    {   
                        Application.displayCustomMacros($data.data);
                        alertify.notify($data.message, 'custom', 4, function(){});
                    }
                    else
                    {
                        alertify.error($data.message, 4);//('Ready!');//console.log("success!");
                    }
                }
            });
            
        });
    },
    
    displayCustomMacros: function($customMacros)
    {   
        console.log($customMacros);
        $('form#calc-macros').before('<div class="col-xs-12">Macros are: <p class="alert alert-success">Proteins: '+$customMacros.custom_proteins+'<br/>Carbs: '+$customMacros.custom_carbohydrates+'<br/>fats: '+$customMacros.custom_fats+'<br/>calories: '+$customMacros.custom_calories+'</p></div>');
        //alert($data);
    },

    initBxSlider: function()
    {
        $('.bxslider').bxSlider({
          captions: false,
          infiniteLoop: true,
          mode: 'vertical',
          speed: 500,
          auto: true,
        });
    },

    activeListItem: function() {
        console.log(window.location.pathname);
       if ( window.location.pathname == '/tmm/' || window.location.pathname == '/'){console.log("test");
            $("#dn-main-menu li:first").addClass("on");
        }
        $("#dn-main-menu li a").click(function(e) {
                var link = $(this);
                var item = link.parent("li");

                if (item.hasClass("on")) {
                    item.removeClass("on");
                } else {
                    item.addClass("on");
                }

                if (item.children("ul").length > 0) {
                    var href = link.attr("href");
                    link.attr("href", "#");
                    setTimeout(function () {
                        link.attr("href", href);
                    }, 300);
                    e.preventDefault();
                }
            }).each(function() {
                var link = $(this);
                if (link.get(0).href === location.href) {
                    link.addClass("on").parents("li").addClass("on");
                    return false;
                }
            });

    },

    centerMobileMenu: function() {
        $( window ).resize(function() {
          if ( $('.dn-main-menu-mobile.vertical-align').length ) {
            $('.dn-main-menu-mobile.vertical-align').parent().css( "text-align", "center" );
          }
        });
        
    }
}


$(document).ready(function () {
    
//    $('body').append('</div><div id="loader"><img src="//www.ajaxload.info/cache/FF/FF/FF/00/00/00/1-0.gif"/></div>');
    //Application.validateMacros();
    //Application.changeLanguage();
    //Application.registerForm();
    //Application.appendLoadingjax();
    //Application.validateCustomMacros();
    //Application.activeListItem();
    Application.centerMobileMenu();
    Application.initBxSlider();
    Application.activeListItem();
});


